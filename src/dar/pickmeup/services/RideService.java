package dar.pickmeup.services;

/**
 * A class to make json object from DB action with DAO
 */
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import dar.pickmeup.dao.EventDao;
import dar.pickmeup.dao.RideDao;
import dar.pickmeup.dao.RideOldDao;
import dar.pickmeup.entities.Ride;
import dar.pickmeup.entities.RideOld;
import dar.pickmeup.util.Tools;

public class RideService {

	/**
	 * Add a ride in DB
	 * @param ride The ride to add
	 * @return A JSONObject with info about the action
	 * {
	 * 		state : [SUCCESS | FAILURE]
	 * 		msg: 
	 * }
	 */
	public static JSONObject addRide(Ride ride) {
		try {
			JSONObject obj;

			if(RideDao.addRide(ride))
				obj = Tools.serviceMessage("Add Ride Success", true);
			else
				obj = Tools.serviceMessage("Add Ride Failure", false);

			return obj;

		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		return  new JSONObject();
	}
	
	/**
	 * Update a ride in DB
	 * @param ride The ride to to update
	 * @return A JSONObject with info about the action
	 * {
	 * 		state : [SUCCESS | FAILURE]
	 * 		msg: 
	 * }
	 */
	public static JSONObject update(Ride ride) {
		try {
			JSONObject obj;

			if(RideDao.updateRide(ride))
				obj = Tools.serviceMessage("Add Update Success", true);
			else
				obj = Tools.serviceMessage("Add Update Failure", false);

			return obj;

		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		return  new JSONObject();
	}
	
	/**
	 * Get all ride in db
	 * @return A json object which is a tab of Ride
	 */
	public static JSONObject getAll() {

		JSONObject obj =  new JSONObject();

		try {

			JSONArray array = new JSONArray();

			List<Ride> rides = RideDao.getAll();
			for(Ride r : rides)
			{
				array.put(Tools.serviceMessage(r));
			}
			obj.put("data", array);

		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		return obj;
	}

	/**
	 * Get a ride in json form
	 * @param id The id of ride
	 * @return A Json object which represente the ride
	 * Example
	 * {
	 * 	idRide    : id
	 *  date      : date
	 *  city      : cite
	 *  time      : time
	 *  owner     : email
	 *  nbPasM	  : nbMax	
	 *  pas       :{[ email1,
	 *  			  email2,
	 *  			  email3
	 *  			]}
	 *  app       :{[ email4,
	 *  			  email5,
	 *  			  email6
	 *  			]}
	 *  eventName : name
	 *  eventCity : cityE
	 *  eventDesc : desc
	 *  eventDateD: dateD
	 *  eventDateF: dateF
	 *  eventAddr : addr
	 *  img 	  : ""
	 * }
	 * 
	 */
	public static JSONObject getRide(int id) {
		try {
			JSONObject obj = new JSONObject();

			Ride ride = RideDao.getRide(id);
			if(ride != null)
				obj = Tools.serviceMessage(ride);
			return obj;
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		return  new JSONObject();
	}


}
