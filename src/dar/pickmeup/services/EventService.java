package dar.pickmeup.services;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import dar.pickmeup.dao.EventDao;
import dar.pickmeup.entities.Event;
import dar.pickmeup.util.Tools;
/**
 * Class to make json object from DB Action with the DAO classes
 * @author Arthur
 *
 */
public class EventService {

	/**
	 * Add an event in DB
	 * @param e The event which will be added
	 * @return A JSONObject with info about the action
	 * {
	 * 		state : [SUCCESS | FAILURE]
	 * 		msg: 
	 * }
	 */
	public static JSONObject addEvent(Event e)
	{
		try {
			JSONObject obj;

			if(EventDao.addEvent(e))
				obj = Tools.serviceMessage("Add Event Success", true);
			else
				obj = Tools.serviceMessage("Add Event Failure", false);

			return obj;

		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		return  new JSONObject();
	}
	/**
	 * Remove an event from DB
	 * @param id The Id of the event which will be removed
	 * @return A JSONObject with info about the action
	 *  * {
	 * 		state : [SUCCESS | FAILURE]
	 * 		msg: 
	 * 	  }
	 */
	public static JSONObject removeEvent(int id)
	{

		try {
			JSONObject obj;

			if(EventDao.removeEvent(id))
				obj = Tools.serviceMessage("Remove Event Success", true);
			else
				obj = Tools.serviceMessage("Remove Event Failure", false);

			return obj;
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		return  new JSONObject();
	}
	
	/**
	 * Get an event from DB
	 * @param id The event id
	 * @return A JSONObjec which represent the event
	 * Example : 
	 * {
   	 *		"geo": "[48.856818,2.677004]",
   	 *		"uid": "84694650",
   	 *		"img": "http://cibul.s3.amazonaws.com/event_vannerie-de-jonc-77_152060.jpg",
	 *	    "city": "Saint-Thibault-des-Vignes",
	 *	    "dateF": "2017-12-09",
	 *	    "name": "Vannerie de Jonc (77)",
	 *	    "idEvent": 99,
	 *	    "dateD": "2017-12-09",
	 *	    "dpt": "Seine-et-Marne",
	 *	    "tag": "jonc,chapeau,ruche,mangeoire,vannerie,decoration,noel",
	 *	    "placename": "Etang de la Loy",
	 *	    "adr": "3 Rue Rene Cassin, 77400 Saint-Thibault-des-Vignes"
	 *	}
	 */
	public static JSONObject getEvent(int id) {

		JSONObject obj = new JSONObject();

		Event event = EventDao.getEvent(id);
		if(event != null)
			obj = Tools.serviceMessage(event);
		return obj;
	}
	/**
	 * Get All Event in DB in a json object
	 * @return A json object which is a tab of event in json
	 */
	public static JSONObject getAll() {

		JSONObject obj =  new JSONObject();
		JSONArray array = new JSONArray();

		List<Event> events = EventDao.getAll();
		for(Event e : events)
		{
			array.put(Tools.serviceMessage(e));
		}

		try {
			obj.put("data", array);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		return obj;
	}
}
