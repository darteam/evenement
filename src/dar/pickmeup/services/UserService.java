package dar.pickmeup.services;

import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import dar.pickmeup.dao.RideDao;
import dar.pickmeup.dao.UserDao;
import dar.pickmeup.entities.User;
import dar.pickmeup.util.Tools;
/**
 *  * A class to make json object from DB action with DAO
 * @author Arthur
 *
 */
public class UserService {

	/**
	 * Add a user in DB
	 * @param user The user to add
	 * @return A JSONObject with info about the action
	 * {
	 * 		state : [SUCCESS | FAILURE]
	 * 		msg: 
	 * }
	 */
	public static JSONObject createUser(User user) {		
		try {
			JSONObject obj;

			if(UserDao.addUser(user))
				obj = Tools.serviceMessage("Create User Success", true);
			else
				obj = Tools.serviceMessage("Create User Failure", false);

			return obj;

		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		return  new JSONObject();
	}


	/**
	 * update a user in DB
	 * @param user The user to update
	 * @return A JSONObject with info about the action
	 * {
	 * 		state : [SUCCESS | FAILURE]
	 * 		msg: 
	 * }
	 */
	public static JSONObject updateUser(User user) {
		try {
			JSONObject obj;

			if(UserDao.updateUser(user))
				obj = Tools.serviceMessage("Update User Success", true);
			else
				obj = Tools.serviceMessage("Update User Failure", false);

			return obj;

		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		return  new JSONObject();
	}
	
	/**
	 * Check if user exist
	 * @param email the email we test
	 * @return A JSONObject with info about the action
	 * {
	 * 		state : [SUCCESS | FAILURE]
	 * 		msg: 
	 * }
	 */
	public static JSONObject checkUserEmailExist(String email){
		try {
			JSONObject obj;

			if(UserDao.checkUserEmailExist(email))
				obj = Tools.serviceMessage("Email Exist", true);
			else
				obj = Tools.serviceMessage("Email doesn't Exist", false);

			return obj;

		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		return  new JSONObject();
	}

	/**
	 * Check user connexion
	 * @param user 
	 * @return A JSONObject with info about the action
	 * {
	 * 		state : [SUCCESS | FAILURE]
	 * 		msg: 
	 * }
	 */
	public static JSONObject checkUserConnexion(User user){
		try {
			JSONObject obj;

			if(UserDao.checkUserConnexion(user))
				obj = Tools.serviceMessage("Connexion Success", true);
			else
				obj = Tools.serviceMessage("Connexion Failure", false);

			return obj;

		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		return  new JSONObject();
	}
	
	/**
	 * Get a user in json form
	 * @param id The id of user
	 * @return A Json object which representes the user
	 * Example
	 * {
	 * 	email      : email
	 *  firstName  : fName
	 *  lastName   : lName
	 *  rides      :{[ id1,
	 *  			   id2,
	 *  			   id3
	 *  			]}
	 *  ridesWait  :{[ id4,
	 *  			      id5,
	 *  			      id6
	 *  			]}
	 *  rideOld	  : {[ id7,
	 *  			   id8,
	 *  			   id9
	 *  			]}	
	 *  notifs 	  : {[ msg1,
	 *  			   msg2,
	 *  			   msg3
	 *  			]}
	 * }
	 * 
	 */
	public static JSONObject getUserInfo(String id)
	{
		try {
			
			JSONObject obj;
			User u = UserDao.getUser(id);
			obj = Tools.serviceMessage(u);
			return obj;
			
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		return  new JSONObject();
	}
	
}
