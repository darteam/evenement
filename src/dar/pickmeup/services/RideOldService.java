package dar.pickmeup.services;


import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import dar.pickmeup.dao.EventDao;
import dar.pickmeup.dao.RideDao;
import dar.pickmeup.dao.RideOldDao;
import dar.pickmeup.entities.Event;
import dar.pickmeup.entities.RideOld;
import dar.pickmeup.entities.User;
import dar.pickmeup.util.Tools;
/**
 * A class to make json object from DB action with DAO
 * @author Arthur
 *
 */
public class RideOldService {


	/**
	 * Add an RideOld  in DB
	 * @param rideOld The ride Old which will be added
	 * @return A JSONObject with info about the action
	 * {
	 * 		state : [SUCCESS | FAILURE]
	 * 		msg: 
	 * }
	 */
	public static JSONObject addRideOld(RideOld rideOld) {
		try {
			JSONObject obj;

			if(RideOldDao.addRideOld(rideOld))
				obj = Tools.serviceMessage("Add RideOld Success", true);
			else
				obj = Tools.serviceMessage("Add RideOld Failure", false);

			return obj;

		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		return  new JSONObject();
	}

	/**
	 * Get all Ride old in DB in json form 
	 * @return A JSONObject which is an array of ride old
	 */
	public static JSONObject getAll() {

		JSONObject obj =  new JSONObject();

		try {

			JSONArray array = new JSONArray();

			List<RideOld> rides = RideOldDao.getAll();
			for(RideOld r : rides)
			{
				array.put(Tools.serviceMessage(r));
			}
			obj.put("data", array);
			
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		return obj;
	}

	/**
	 * Get a ride old in json form
	 * @param id The id of ride old
	 * @return A Json object which represente the ride old 
	 * Example
	 * {
	 * 	idRide    : id
	 *  date      : date
	 *  city      : cite
	 *  owner     : email
	 *  pas       :{[ email1,
	 *  			  email2,
	 *  			  email3
	 *  			]}
	 *  eventName : name
	 *  eventCity : cityE
	 *  eventDesc : desc
	 *  img 	  : ""
	 * }
	 * 
	 */
	public static JSONObject getRideOld(int id) {
		try {
			JSONObject obj = new JSONObject();

			RideOld ride = RideOldDao.getRideOld(id);
			if(ride != null)
				obj = Tools.serviceMessage(ride);
			return obj;
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		return  new JSONObject();
	}

}
