package dar.pickmeup.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import dar.pickmeup.entities.Event;
import dar.pickmeup.entities.Ride;
import dar.pickmeup.util.HibernateUtil;

/**
 * DAO for the entities Ride, use for getting Ride Object in the data base
 * @author Arthur
 *
 */
public class RideDao {

	/**
	 * Add a Ride in the DB.
	 * The id of Ride will be set by hibernate
	 * @param ride The Ride which will be added in the DB
	 * @return true on success, false otherwise
	 */
	public static boolean addRide(Ride ride) {
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		boolean result = false;
		
			try {
				trns = session.beginTransaction();
				session.save(ride);
				trns.commit();
				result=true;
			} catch (Exception e) {
				if (trns != null) {
					trns.rollback();
				}
				e.printStackTrace();
			} finally {
				session.close();
			}
		
		return result;
	}
	
	/**
	 * Get all ride in the DB
	 * @return A list with all event in the DB
	 */
	public static List<Ride> getAll() {
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		List<Ride> result = null;
			try {
				trns = session.beginTransaction();
	
				CriteriaBuilder cb = session.getCriteriaBuilder();
				CriteriaQuery<Ride> query = cb.createQuery(Ride.class);
				Root<Ride> ridesR = query.from(Ride.class);
				query.select(ridesR);
				TypedQuery<Ride> q = session.createQuery(query);
				result = q.getResultList();
				
				trns.commit();
				
			} catch (Exception e) {
				if (trns != null) {
					trns.rollback();
				}
				e.printStackTrace();
			} finally {
				session.close();
			}
		
		return result;
	}

	/**
	 * Remove a ride from the DB
	 * @param id The Ride id which will be deleted from DB 
	 * @return true on success, false otherwise
	 */
	public static boolean removeRide(int id)
	{
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		boolean result = false;
		
			try {
				trns = session.beginTransaction();
				Ride r = session.get(Ride.class,id);
				session.remove(r);
				trns.commit();
				result =true;
			} catch (Exception e) {
				if (trns != null) {
					trns.rollback();
				}
				e.printStackTrace();
			} finally {
				session.close();
			}
	
		return result;
	}

	/**
	 * Get a ride from is id
	 * @param id The Ride id
	 * @return The Ride with the id given in parameter null otherwise
	 */
	public static Ride getRide(int id)
	{
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		Ride result = null;
		
			try {
				trns = session.beginTransaction();
				Ride r = session.get(Ride.class,id);
				trns.commit();
				result = r;
			} catch (Exception e) {
				if (trns != null) {
					trns.rollback();
				}
				e.printStackTrace();
			} finally {
				session.close();
			}
	
		return result;
	}
	
	/**
	 * Update a ride
	 * @param r The ride that you want update
	 * @return true on success false otherwise
	 */
	public static boolean updateRide(Ride r) {
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		boolean result = false;
		
			try {
				trns = session.beginTransaction();
				session.update(r);
				session.getTransaction().commit();
				result = true;
			} catch (Exception e) {
				if (trns != null) {
					trns.rollback();
				}
				e.printStackTrace();
			} finally {
				session.close();
			}
		
		return result;
	}
	
	/**
	 * Get Rides which go to the event Event
	 * @param ev The destination of the ride
	 * @return A list with Rides which go to ev
	 */
	public static List<Ride> getRideByEvent(Event ev){
		Transaction trns = null;
		List<Ride> rides=new ArrayList<Ride>();
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {

        	trns=session.beginTransaction();
        
        	Query query = session.createQuery("from Ride where event = :event");
     			query.setParameter("event", ev);
     			try{
     				rides.addAll((List<Ride>)query.getResultList());
     				trns.commit();
     			}
     			catch(Exception e)
     			{
     				System.err.println("No result:  select * from rides from Ride where event = "+ev.getIdEvent());
     			}
     			
     		
			
        	
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
		return rides;
	}
	
	/**
	 * Check if a ride exist
	 * @param id the Ride id
	 * @return true if ride exist false otherwise
	 */
	public static boolean rideExist(int id)
	{
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		boolean result = true;

		try {
			Query query = session.createQuery("from Ride where id = :id");
			query.setParameter("id", id);

			List<Ride> list = (List<Ride>)query.getResultList();

			trns = session.beginTransaction();
			trns.commit();
			if(list.size() != 0)
				result =  true;
			else
				result = false;

		} catch (Exception e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}

		return result;
	}
	
	/**
	 * Get all Rides which go to EventName from city and not created by idUser 
	 * @param EventName Name of event destination
	 * @param city The Start City
	 * @param idUser The User id
	 * @return A List of all ride which match with parameter
	 * 
	 */
	public static List<Ride> getRideByEventName(String EventName, String city,String idUser){
		Transaction trns = null;
		List<Ride> rides=new ArrayList<Ride>();
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		
		 try {
	        	List<Integer> eventsIds=EventDao.getEventByName(EventName);
	           	
	        	trns=session.beginTransaction();
	        	
	        	
	        	String sql ="SELECT idride FROM rides WHERE city like :cityParam";
	        	/*String sql = "SELECT idride FROM rides where "+((eventsIds.size() != 0)?" ( ":"");
	        	int cpt = 0;
	        	
	        	for (Integer eventId : eventsIds) {
					sql+=" event = "+eventId;
					cpt++;
					if(eventsIds.indexOf(eventId)!=eventsIds.size()-1){
						sql+=" OR ";
					}
				}
	        	
	        	sql+=((eventsIds.size() != 0)?" ) AND ":"")+" */
	        	
	        	SQLQuery query = (SQLQuery) session.createSQLQuery(sql).setString("cityParam","%"+city+"%");
	        	
	        	query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
	        	List<Map> result = query.getResultList();
	        	for (Map map : result) {
	        		Ride ride=session.get(Ride.class,(Integer)map.get("idride"));
	        		if(eventsIds.contains(ride.getEvent().getIdEvent()) && !ride.getOwner().getEmail().equals(idUser))
	        			rides.add(ride);
				}        	
	        } catch (RuntimeException e) {
	            if (trns != null) {
	                trns.rollback();
	            }
	            e.printStackTrace();
	        } finally {
	            session.close();
	        }
		return rides;
	}
}
