package dar.pickmeup.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import dar.pickmeup.entities.Event;
import dar.pickmeup.util.HibernateUtil;

/**
 * DAO for the entities Event, use for getting Event Object in the data base
 * @author Arthur
 *
 */
public class EventDao {

	/**
	 * Add an object Event in the data base.
	 * The event id will be set by hibernate.
	 * @param event Object which will be add in the data base.
	 * @return true if the operation is a success, otherwise false
	 */
	public static boolean addEvent(Event event) {
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		boolean result = false;

		try {
			trns = session.beginTransaction();
			session.save(event);
			trns.commit();
			result = true;
		} catch (Exception e) {
			if (trns != null) {
				trns.rollback();
			}

			e.printStackTrace();
		} finally {
			session.close();
		}

		return result;
	}

	/**
	 * Remove an event from the data base
	 * @param id The event id which will be deleted 
	 * @return true if the operation is a success, otherwise false
	 */
	public static boolean removeEvent(int id)
	{
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		boolean result = false;

		try {
			trns = session.beginTransaction();
			Event e = session.get(Event.class,id);
			e.setIdEvent(id);
			session.remove(e);
			trns.commit();
			result =true;
		} catch (Exception e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}

		return result;
	}

	/**
	 * Get the an event
	 * @param id The id of the event that your looking for
	 * @return The event with the id given in parameter null otherwise
	 */
	public static Event getEvent(int id)
	{
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		Event result = null;

		try {
			trns = session.beginTransaction();
			Event e = session.get(Event.class,id);
			trns.commit();
			result = e;
		} catch (Exception e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}

		return result;
	}

	/**
	 * Return all event in the DB
	 * @return A list with all events in the DB
	 */
	public static List<Event> getAll() {
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		List<Event> result = null;

		try {
			trns = session.beginTransaction();

			String hql = "FROM dar.pickmeup.entities.Event";
			Query query = session.createQuery(hql);

			result = (List<Event>)query.getResultList();
			trns.commit();

		} catch (Exception e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}

		return result;
	}

	/**
	 * check if an event exist with the uid in the DB.
	 * User for knowing if an event from the API is already 
	 * in the DB
	 * @param uid UID is defined in the API, 
	 * @return true if an event with uid exists, false otherwise
	 */
	public static boolean eventExist(String uid)
	{
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		boolean result = true;

		try {
			Query query = session.createQuery("from Event where uid = :uid");
			query.setParameter("uid", uid);

			List<Event> list = (List<Event>)query.getResultList();

			trns = session.beginTransaction();
			trns.commit();
			if(list.size() != 0)
				result =  true;
			else
				result = false;

		} catch (Exception e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}

		return result;
	}

	/**
	 * Get all event's id which have a name close to the parameter name
	 * @param name A name if the event name seems similar to it the event id will be added in the list
	 * @return A list with the events id which have a name which looks like parameter name
	 */
	public static List<Integer> getEventByName(String name){
		Transaction trns = null;
		List<Integer> idEvents=null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			String sql = "SELECT idEvent FROM events where name like :eventNameParam";
			SQLQuery query = session.createSQLQuery(sql).setParameter("eventNameParam","%"+name+"%");
			query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			List results = query.list();
			idEvents=new ArrayList<Integer>();
			for (Object object : results) {
				Map map=(Map)object;
				idEvents.add((Integer)map.get("idevent"));
			}

		} catch (RuntimeException e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		} finally {
			//session.flush();
			session.close();
		}
		return idEvents;
	}


	/**
	 * Get all event name which are close to the parameter name
	 * @param name A name if the event name seems similar to it the event's name will be added in the list
	 * @return A list with the events name which have a name which looks like parameter name
	 */

	public static List<String> getEventsNamesByName(String name){
		Transaction trns = null;
		List<String> EventsNames=null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			//TO DO: utiliser LIKE au lieu de EQUA dans les requetes ! 
			String sql = "SELECT name FROM events where name like :eventNameParam";
			SQLQuery query = session.createSQLQuery(sql).setParameter("eventNameParam","%"+name+"%");
			query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			List results = query.list();
			EventsNames=new ArrayList<String>();
			for (Object object : results) {
				Map map=(Map)object;
				EventsNames.add((String)map.get("name"));
			}

		} catch (RuntimeException e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		} finally {
			//session.flush();
			session.close();
		}
		return EventsNames;
	}

	/**
	 * Get the name of each event in the DB
	 * @return A list with the name of each event
	 */
	public static List<String> getAllEventsNames(){
		Transaction trns = null;
		List<String> allEventsNames=null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			String sql = "SELECT name FROM events";
			SQLQuery query = session.createSQLQuery(sql);
			query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			List results = query.list();
			allEventsNames=new ArrayList<String>();
			for (Object object : results) {
				Map map=(Map)object;
				allEventsNames.add((String)map.get("name"));
			}

		} catch (RuntimeException e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		} finally {
			//session.flush();
			session.close();
		}
		return allEventsNames;
	}
	
	/**
	 *Get the event id with the name name
	 * @param name the that you are looking for
	 * @return The id of the event which has the name name
	 */
	public static int getIdEventByName(String name){
		Transaction trns = null;
		List<Integer> eventsNames=null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			String sql = "SELECT idevent FROM events WHERE name=:eventNameParam";
			SQLQuery query = session.createSQLQuery(sql).setParameter("eventNameParam",name);
			query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			List results = query.list();
			eventsNames=new ArrayList<Integer>();
			for (Object object : results) {
				Map map=(Map)object;
				eventsNames.add((Integer)map.get("idevent"));
			}

		} catch (RuntimeException e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		} finally {
			//session.flush();
			session.close();
		}
		return eventsNames.get(0);
	}

}
