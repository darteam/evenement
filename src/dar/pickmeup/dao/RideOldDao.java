package dar.pickmeup.dao;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;

import dar.pickmeup.entities.Ride;
import dar.pickmeup.entities.RideOld;
import dar.pickmeup.util.HibernateUtil;

/**
 * DAO for the entities RideOld, use for getting RideOld Object in the data base
 * @author Arthur
 *
 */

public class RideOldDao {

	/**
	 * Add a RideOld the DB,
	 * The user need to set the RideOld id 
	 * @param rideOld The RideOld which will be added in the DB
	 * @return true on success false otherwise
	 */
	public static boolean addRideOld(RideOld rideOld) {
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		boolean result = false;
		
			try {
				trns = session.beginTransaction();
				session.save(rideOld);
				trns.commit();
				result=true;
			} catch (Exception e) {
				if (trns != null) {
					trns.rollback();
				}
				e.printStackTrace();
			} finally {
				session.close();
			}
		
		return result;
	}
	
	/**
	 * Get All RideOld in the DB
	 * @return A list which contains all RideOld
	 */
	public static List<RideOld> getAll() {
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		List<RideOld> result = null;
			try {
				trns = session.beginTransaction();
	
				CriteriaBuilder cb = session.getCriteriaBuilder();
				CriteriaQuery<RideOld> query = cb.createQuery(RideOld.class);
				Root<RideOld> RideOldsR = query.from(RideOld.class);
				query.select(RideOldsR);
				TypedQuery<RideOld> q = session.createQuery(query);
				result = q.getResultList();
				
				trns.commit();
				
			} catch (Exception e) {
				if (trns != null) {
					trns.rollback();
				}
				e.printStackTrace();
			} finally {
				session.close();
			}
		
		return result;
	}

	/**
	 * Remove a RideOld from DB
	 * @param id The id of the RideOld which will be deleted from DB
	 * @return true on success, false otherwise
	 */
	public static boolean removeRideOld(int id)
	{
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		boolean result = false;
		
			try {
				trns = session.beginTransaction();
				RideOld r = session.get(RideOld.class,id);
				r.setIdRide(id);
				session.remove(r);
				trns.commit();
				result =true;
			} catch (Exception e) {
				if (trns != null) {
					trns.rollback();
				}
				e.printStackTrace();
			} finally {
				session.close();
			}
		return result;
	}

	/**
	 * 
	 * Get a RideOld
	 * @param id the id of the RideOld that you want to get back
	 * @return The RideOld with the id given in parameter null otherwise
	 */
	public static RideOld getRideOld(int id)
	{
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		RideOld result = null;
		
			try {
				trns = session.beginTransaction();
				RideOld r = session.get(RideOld.class,id);
				trns.commit();
				result = r;
			} catch (Exception e) {
				if (trns != null) {
					trns.rollback();
				}
				e.printStackTrace();
			} finally {
				session.close();
			}
	
		return result;
	}
	
	/**
	 * Update a RideOld 
	 * @param r The RideOld which will be updated
	 * @return true on success, false otherwise
	 */
	public static boolean updateRideOld(RideOld r) {
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		boolean result = false;
		
			try {
				trns = session.beginTransaction();
				session.update(r);
				session.getTransaction().commit();
				result = true;
			} catch (Exception e) {
				if (trns != null) {
					trns.rollback();
				}
				e.printStackTrace();
			} finally {
				session.flush();
				session.close();
			}
		
		return result;
	}
}
