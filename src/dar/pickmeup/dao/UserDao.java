package dar.pickmeup.dao;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONObject;

import dar.pickmeup.entities.Ride;
import dar.pickmeup.entities.User;
import dar.pickmeup.util.HibernateUtil;


/**
 * DAO for the entities User, use for getting User Object in the data base
 * @author Arthur
 *
 */
public class UserDao {

	/**
	 * Add a User in the DB
	 * @param user The user which will be added in the DB
	 * @return true on success, false otherwise
	 */
	public static boolean addUser(User user) {
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		boolean result = false;
		
			try {
				trns = session.beginTransaction();
				session.save(user);
				trns.commit();
				result = true;
			} catch (RuntimeException e) {
				if (trns != null) {
					trns.rollback();
				}
				
				e.printStackTrace();
			} finally {
				session.close();
			}
		
		return result;
	}

	/**
	 * Update a User in DB
	 * @param user The user which will be updated
	 * @return true on success, false otherwise
	 */
	public static boolean updateUser(User user) {
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		boolean result = false;
		
			try {
				trns = session.beginTransaction();
				session.update(user);
				session.getTransaction().commit();
				result = true;
			} catch (Exception e) {
				if (trns != null) {
					trns.rollback();
				}
				e.printStackTrace();
			} finally {
				//session.flush();
				session.close();
			}
		
		return result;
	}

	/**
	 * Check an email already exist
	 * @param email
	 * @return true if a user use the email given in parameter
	 */
	public static boolean checkUserEmailExist(String email)
	{
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		User u = null;
		boolean result= true;

		try {
			trns = session.beginTransaction();
			u = session.get(User.class,email);
			session.getTransaction().commit();

			if(u == null)
				result = false;
			
		} catch (Exception e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return result;

	}

	/**
	 * Check if the User is allowed to connect on the server
	 * The User should have the good email associate with the good password.
	 * @param user The user which want to connect
	 * @return true on success false otherwise
	 */
	public static boolean checkUserConnexion(User user)
	{
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		boolean result = false;

		try {
			trns = session.beginTransaction();
			User u = session.get(User.class,user.getEmail());
			session.getTransaction().commit();
			if(u != null && u.getPassword().compareTo(user.getPassword()) == 0)
				result = true;
		} catch (Exception e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		} finally {
		
			session.close();
		}
		return result;
	}
	
	/**
	 * Get a user
	 * @param email the user eamil
	 * @return the user with the given email null otherwise
	 */
	public static User getUser(String email)
	{
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		User result = null;

		try {
			trns = session.beginTransaction();
			result = session.get(User.class,email);
			session.getTransaction().commit();
		} catch (Exception e) {
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return result;
	}
	
	/**
	 * Get all user in DB
	 * @return A list with all users in DB
	 */
	public static List<User> getAll() {
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		List<User> result = null;
			try {
				trns = session.beginTransaction();
	
				CriteriaBuilder cb = session.getCriteriaBuilder();
				CriteriaQuery<User> query = cb.createQuery(User.class);
				Root<User> userR = query.from(User.class);
				query.select(userR);
				TypedQuery<User> q = session.createQuery(query);
				result = q.getResultList();
				
				trns.commit();
				
			} catch (Exception e) {
				if (trns != null) {
					trns.rollback();
				}
				e.printStackTrace();
			} finally {
				session.close();
			}
		
		return result;
	}
}
