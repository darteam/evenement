package dar.pickmeup.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;

import dar.pickmeup.entities.Ride;
import dar.pickmeup.entities.TrackUser;
import dar.pickmeup.util.HibernateUtil;

public class TrackUserDao {

	
	public static void saveTrackUser(TrackUser info) {
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		
			try {
				trns = session.beginTransaction();
				session.save(info);
				trns.commit();
			} catch (Exception e) {
				if (trns != null) {
					trns.rollback();
				}
				e.printStackTrace();
			} finally {
				session.close();
			}
		
	}
	
	public static void updateTrackUser(TrackUser info) {
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		
			try {
				trns = session.beginTransaction();
				session.update(info);
				trns.commit();
			} catch (Exception e) {
				if (trns != null) {
					trns.rollback();
				}
				e.printStackTrace();
			} finally {
				session.close();
			}
		
	}
	
	public static TrackUser getTrackUser(String token) {
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		TrackUser result = null;
		
			try {
				trns = session.beginTransaction();
				TrackUser info = session.get(TrackUser.class,token);
				trns.commit();
				result = info;
			} catch (Exception e) {
				if (trns != null) {
					trns.rollback();
				}
				e.printStackTrace();
			} finally {
				session.close();
			}
	
		return result;
	}
}
