package dar.pickmeup.entities;

import java.io.File;
import java.util.Calendar;
import java.util.List;

import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.webresources.DirResourceSet;
import org.apache.catalina.webresources.StandardRoot;

import dar.pickmeup.test.TestBDD;
import dar.pickmeup.util.BDDUpdate;
import dar.pickmeup.util.ExternAPIConnexion;

public class Main {

    public static void main(String[] args) throws Exception {

        String webappDirLocation = "WebContent";
        Tomcat tomcat = new Tomcat();

        //The port that we should run on can be set into an environment variable
        //Look for that variable and default to 8080 if it isn't there.
        String webPort = System.getenv("PORT");
        if(webPort == null || webPort.isEmpty()) {
            webPort = "8080";
        }

        tomcat.setPort(Integer.valueOf(webPort));

        StandardContext ctx = (StandardContext) tomcat.addWebapp("/", new File(webappDirLocation).getAbsolutePath());
        System.out.println("configuring app with basedir: " + new File("./" + webappDirLocation).getAbsolutePath());

        // Declare an alternative location for your "WEB-INF/classes" dir
        // Servlet 3.0 annotation will work
        File additionWebInfClasses = new File("target/classes");
        WebResourceRoot resources = new StandardRoot(ctx);
        resources.addPreResources(new DirResourceSet(resources, "/WEB-INF/classes",
                additionWebInfClasses.getAbsolutePath(), "/"));
        ctx.setResources(resources);

        System.out.println("Depart serveur !!");
        Calendar date = Calendar.getInstance();
        date.set(Calendar.HOUR_OF_DAY,1);
        date.set(Calendar.MINUTE,0);
     
        BDDUpdate.init();
        BDDUpdate.update();
        BDDUpdate.getSingleton().keepUpdate(date,1000*60*60*24);
        
        tomcat.start();
        tomcat.getServer().await();
    }
}
