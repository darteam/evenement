package dar.pickmeup.entities;

import java.util.HashSet;
import java.util.Set;
/**
 * The class for Ride entitie
 * @author Arthur
 *
 */
public class Ride {

	/**
	 * The Ride id
	 */
	Integer idRide;
	/**
	 * The event destination of the ride
	 */
	Event event;
	/**
	 * The user who has created the ride
	 */
	User owner;
	/**
	 * The maximum number of passengers
	 */
	Integer MaxNbPassengers;
	/**
	 * The number of passengers
	 */
	Integer nbPassengers;
	/**
	 * The start city of the ride
	 */
	String city;
	/**
	 * The set of user who participate to the ride
	 */
	Set<User> passengers;
	/**
	 * The set of user who ask to participate to the ride
	 */
	Set<User> applicants;
	/**
	 * The ride date
	 */
    String date;
    /**
     * The ride time
     */
    String time;
    
    /**
     * Default constructor
     */
    public Ride()
    {
          passengers = new HashSet<User>();
          applicants = new HashSet<User>();
    }
	
    /**
     * 
     * @param event The event destionation
     * @param owner The user creator
     * @param maxNbPassengers the maximum number of passenger
     * @param city The start city
     * @param date The date of the ride
     * @param time the ride time
     */
	public Ride(Event event, User owner, Integer maxNbPassengers, String city, String date, String time) {
		super();
		this.event = event;
		this.owner = owner;
		this.MaxNbPassengers = maxNbPassengers;
		this.city = city;
		this.date = date;
		this.time = time;
		passengers = new HashSet<User>();
        applicants = new HashSet<User>();
        nbPassengers = 0;
	}
	
	/**
	 * The attribute date will have the value event.getDateD()
	 * The attribute time will have the value "20:00"
	 * @param event The event destination
	 * @param owner The user creator
	 * @param maxNbPassengers The maximum number of passengers
	 * @param city The start city
	 */
	public Ride(Event event, User owner, Integer maxNbPassengers,String city) {
		super();
		this.event = event;
		this.owner = owner;
		this.MaxNbPassengers = maxNbPassengers;
		this.city = city;
		this.date=event.getDateD();
		this.time="20:00";
		passengers = new HashSet<User>();
        applicants = new HashSet<User>();
        nbPassengers = 0;
	}

	/**
	 * Get the id ride
	 * @return
	 */
	public Integer getIdRide() {
		return idRide;
	}
	/**
	 * set the is ride - You should not use this, Yhis is for hibernate
	 * @param idRide  the new idRide
	 */ 
	public void setIdRide(Integer idRide) {
		this.idRide = idRide;
	}
	/**
	 *Get event destination
	 * @return The event destination
	 */
	
	public Event getEvent() {
		return event;
	}
	/**
	 * Set the event destination
	 * @param event The event destination
	 */
	public void setEvent(Event event) {
		this.event = event;
	}
	/**
	 * Get The user who own the ride
	 * @return The ride owner 
	 */
	public User getOwner() {
		return owner;
	}
	/**
	 * Set the ride owner
	 * @param owner The new owner
	 */
	public void setOwner(User owner) {
		this.owner = owner;
	}
	/**
	 * Get the nbMax of passenger
	 * @return The nbMax of passengers
	 */
	public Integer getMaxNbPassengers() {
		return MaxNbPassengers;
	}
	/**
	 * Set the nbMax of passenger
	 *@param maxNbPassengers The new nbMax of passengers
	 */
	public void setMaxNbPassengers(Integer maxNbPassengers) {
		MaxNbPassengers = maxNbPassengers;
	}
	/**
	 * Get the nb of passengers
	 * @return The nb of passengers
	 */
	public Integer getNbPassengers() {
		return passengers.size();
	}
	/**
	 * Do nothing this is for hibernate
	 * @param nbPassengers
	 */
	public void setNbPassengers(Integer nbPassengers) {
		this.nbPassengers =nbPassengers; 
	}
	/**
	 * Get the ride city
	 * @return The ride city
	 */
	public String getCity()
	{
		return city;
	}
	/**
	 * Set the ride city
	 * @param city The new city
	 */
	public void setCity(String city)
	{
		this.city = city;
	}
	/**
	 * Get the ride date
	 * @return The ride date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * Set the ride date
	 * @param date The new date
	 */
	public void setDate(String date) {
		this.date = date;
	}
	/**
	 * Get the ride time
	 * @return The ride time
	 */
	public String getTime() {	
		return time;
	}
	/**
	 * Set the ride time
	 * @param time the new ride time
	 */
	public void setTime(String time) {
		this.time = time;
	}
	/**
	 * Get The passenger Set
	 * @return The passenger Set
	 */
	public Set<User> getPassengers() {
		return passengers;
	}
	/**
	 * Set the passengers Set
	 * @param passengers The new passenger Set
	 */
	public void setPassengers(Set<User> passengers) {
		this.passengers = passengers;
	}
	/**
	 * Get applicants Set
	 * @return The applicants set
	 */
	public Set<User> getApplicants() {
		return applicants;
	}
	/**
	 * Set the applicants set
	 * @param applicants The new applicant set
	 */
	public void setApplicants(Set<User> applicants) {
		this.applicants = applicants;
	}
	/**
	 * Add passenger in the ride
	 * @param user The new passenger
	 * @throws Exception if the ride is fill an exception is thrown
	 */
	public void addPassenger(User user) throws Exception
	{
		if(getNbPassengers() == getMaxNbPassengers())
			throw new Exception("Trajet Complet plus de place");
		else
		{
			if(passengers.contains(user))
				throw new Exception(user.getEmail()+" already in ride");
			passengers.add(user);
		}
	}
	/**
	 * Remove a passenger from ride
	 * @param user The user who leave the ride
	 * @throws Exception If the user is not a passengers an exception is thrown
	 */
	public void removePassenger(User user) throws Exception
	{
		if(!passengers.contains(user))
			throw new Exception(user.getEmail()+" is not in ride");

		passengers.remove(user);
	}
	/**
	 * Add an applicant for the ride
	 * @param user The new applicant
	 * @throws Exception If the user is already an applicant an exception is thrown
	 */
	public void addApplicants(User user) throws Exception
	{
		if(applicants.contains(user))
			throw new Exception(user.getEmail()+" already in applicant");
		applicants.add(user);
	}
	/**
	 * Remove an applicant 
	 * @param user The applicant who will be removed
	 * @throws Exception If the user is not an applicant an exception is thrown
	 */
	public void removeApplicants(User user) throws Exception
	{
		if(!applicants.contains(user))
			throw new Exception(user.getEmail()+" not an applicant");
		applicants.remove(user);
	}
	/**
	 * Return true id o is a ride and this.getIdRide() == o.getIdRide()
	 * Otherwise false
	 */
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Ride)
		{
			Ride r = (Ride)o;
			
			if(idRide != r.getIdRide())
				return false;
			return true;
			
		}
		else 
			return super.equals(o);
	}
	
}
