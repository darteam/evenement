package dar.pickmeup.entities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * The class for the entitie User
 * @author Arthur
 *
 */
public class User {
	
	/**
	 * The user first name 
	 */
	private String firstName;
	/**
	 * The user last name
	 */
	private String lastName;
	/**
	 * The user email 
	 */
	private String email;
	/**
	 * The user password
	 */
	private String password;
	/**
	 * The ride set where the user participate
	 */
	private Set<Ride> rides;
	/**
	 * The ride old set where the user has participated
	 */
	private Set<RideOld> ridesOld;
	/**
	 * The ride set where the user want to participate
	 */
	private	Set<Ride> ridesWait;
	/**
	 * The user notifications
	 */
	private Set<String> notifs;
	
	/**
	 * Default constructor
	 */
	public User()
	{
		rides = new HashSet<Ride>();
		ridesOld = new HashSet<RideOld>();
		ridesWait = new HashSet<Ride>();
		notifs = new HashSet<String>();
	}
	
	/**
	 * 
	 * @param email The user email
	 * @param password The user password
	 * @param lastName The user last Name
	 * @param firstname The user first name
	 */
	public User(String email, String password, String lastName, String firstname) {
		
		this.email = email;
		this.password = password;
		this.firstName = firstname;
		this.lastName = lastName;
		
		rides = new HashSet<Ride>();
		ridesOld = new HashSet<RideOld>();
		ridesWait = new HashSet<Ride>();	
		notifs = new HashSet<String>();
	}
	/**
	 * Get The user first name
	 * @return The user first name
	 */
	public String getFirstName() {
		return firstName;
	}	
	/**
	 * Set the user first name
	 * @param firstName The new first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * Get the user last name
	 * @return The user last name
	 */
	
	public String getLastName() {
		return lastName;
	}
	/**
	 * Set the user last name
	 * @param lastName The new last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * Get the user email
	 * @return The user email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * Set the user email
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * Get the user password
	 * @return the user password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * 
	 * Set the user password
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * Get the ride set
	 * @return the ride set
	 */
	public Set<Ride> getRides() {
		return rides;
	}
	/**
	 * Set the ride set
	 * @param rides The new ride set
	 */
	public void setRides(Set<Ride> rides) {
		this.rides = rides;
	}
	/***
	 * Get the rideold set
	 * @return The ride oldset
	 */
	public Set<RideOld> getRidesOld() {
		return ridesOld;
	}
	/**
	 * Set the ride old set
	 * @param ridesOld the new ride old set
	 */
	public void setRidesOld(Set<RideOld> ridesOld) {
		this.ridesOld = ridesOld;
	}
	/**
	 * Get ride wait set
	 * @return The ride wait set
	 */
	public Set<Ride> getRidesWait() {
		return ridesWait;
	}
	/**
	 * Set the ride wait set
	 * @param ridesWait The new ride wait set
	 */
	public void setRidesWait(Set<Ride> ridesWait) {
		this.ridesWait = ridesWait;
	}
	/**
	 * Get the notifs set
	 * @return The notif set
	 */
	public Set<String> getNotifs() {
		return notifs;
	}
	/**
	 * Set the notif set
	 * @param notifs The new notif set
	 */
	public void setNotifs(Set<String> notifs) {
		this.notifs = notifs;
	}
	/**
	 * Add a ride in Ride set
	 * @param r The ride to add
	 * @throws Exception If the ride is already the the set an exception is thrown
	 */
	public void addRide(Ride r) throws Exception
	{
		if(rides.contains(r))
			throw new Exception(r.getIdRide()+" Already exist in Ride set  for "+email);
		
		rides.add(r);
	}
	
	/**
	 * Add a ride old in ride old set
	 * @param r The ride old to add
	 * @throws Exception  If the ride old is already in the ride old set an exception is thrown
	 */
	public void addRideOld(RideOld r) throws Exception
	{
		if(ridesOld.contains(r))
			throw new Exception(r.getIdRide()+" Already exist in RideOld set for "+email);
		
		ridesOld.add(r);
	}
	/**
	 * Add Ride in ride wait set
	 * @param r The ride to add
	 * @throws Exception If the ride is already in the set an exception is thrown
	 */
	public void addWaited(Ride r) throws Exception
	{
		if(ridesWait.contains(r))
			throw new Exception(r.getIdRide()+" Already exist in RideWait set for "+email);
		
		ridesWait.add(r);
	}
	
	/**
	 * Add an notif in the set
	 * @param notif the new notif
	 */
	public void addNotif(String notif)
	{
		notifs.add(notif);
	}
	/**
	 * Remove a ride from ride set
	 * @param r The ride to delete
	 * @throws Exception If the ride is not in the ride set
	 */
	public void removeRide(Ride r) throws Exception
	{
		if(!rides.contains(r))
			throw new Exception(r.getIdRide()+"don't exist in Ride set for "+email);
		rides.remove(r);
	}
	/**
	 * Remove a ride from ride wait set
	 * @param r The ride to delete
	 * @throws Exception If the ride is not in the ride wait set an exception is thrown
	 */
	public void removeRideWait(Ride r) throws Exception
	{
		if(!ridesWait.contains(r))
			throw new Exception(r.getIdRide()+"don't exist in Ride Wait setfor "+email);
		ridesWait.remove(r);
	}
	/**
	 * Remove from notifs all notif which contains id
	 * @param id 
	 * @throws Exception
	 */
	public void removeNotif(String id) throws Exception
	{	
		ArrayList<String>list = new ArrayList<String>();
		for(String notif : notifs)
		{
			if(notif.contains(id))
				list.add(notif);
		}
		
		for(String notif: list)
			notifs.remove(notif);
	}
	
	/**
	 * Remove a ride from ride set and put a ride old in ride old set
	 * @param r The ride which will be removed in ride set
	 * @param ro The ride old which will be added i ride old set
	 * @throws Exception If r not in ride is not in ride set or if ro is in ride old set
	 */
	public void moveToOldRide(Ride r, RideOld ro) throws Exception
	{
		removeRide(r);
		addRideOld(ro);
	}
	/**
	 * Return true if obj is a user and if this.email == obj.getEmail()
	 */
	@Override
	public boolean equals(Object obj)
	{
		if(obj instanceof User)
		{
			User u = (User)obj;
			
			if(!email.equals(u.getEmail()))
				return false;
		
			return true;
		}
		else
			return super.equals(obj);
	}
}
