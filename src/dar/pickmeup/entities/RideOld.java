package dar.pickmeup.entities;

import java.util.HashSet;
import java.util.Set;
/**
 * The class for the entitie RideOld
 * When a ride is out dated it's become a RideOld
 * @author Arthur
 *
 */
public class RideOld {
	/**
	 * The RideOld id
	 */
	Integer idRide;
	/**
	 * The RideOld creator
	 */
	User owner;
	/**
	 * The RideOld passenger
	 */
    Set<User> passengers;
    /**
     * The RideOld city
     */
	String city;
	/**
	 * The RideOld date
	 */
	String date;
	/**
	 * The ride old time
	 */
    String hour;
    /**
     * The name of the event where the RideOld went
     */
    String nameEvent;
    /**
     * The city of the event where the RideOld went
     */
    String cityEvent;
    /**
     * The description of the event where the RideOld went
     */
    String descEvent;
    /**
     * Default constructor
     */
	public RideOld() {
		super();
		passengers = new HashSet<User>();
	}
	/**
	 * Create The RideOld from the Ride 
	 * @param r The Ride
	 */
	public RideOld(Ride r) {
		super();
		
		this.idRide = r.getIdRide();
		this.owner = r.getOwner();
		this.city = r.getCity();
		this.date=r.getDate();
		Event e = r.getEvent();
		if(e == null)
			e = new Event();
		this.nameEvent = e.getName();
		this.cityEvent = e.getCity();
		this.descEvent =e.getdescription();
		passengers = r.getPassengers();
	}
	/**
	 * Get the RideOld id
	 * @return The RideOld id
	 */
	public Integer getIdRide() {
		return idRide;
	}
	/**
	 * Set the RideOld id
	 * @param idRide the new RideOld id
	 */
	public void setIdRide(Integer idRide) {
		this.idRide = idRide;
	}
	/**
	 * Get the RideOld owner
	 * @return the RideOld owner
	 */
	public User getOwner() {
		return owner;
	}
	/**
	 * Set the RideOld owner
	 * @param owner The new owner
	 */
	public void setOwner(User owner) {
		this.owner = owner;
	}
	/**
	 * Get the RideOld passengers
	 * @return The RideOld passengers
	 */
	public Set<User> getPassengers() {
		return passengers;
	}
	/**
	 * Set the rideOld passengers
	 * @param passengers The new passengers
	 */
	public void setPassengers(Set<User> passengers) {
		this.passengers = passengers;
	}
	/**
	 * Get the RideOld city
	 * @return The RiedOld city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * Set the RideOld city
	 * @param city The new city
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * Get the RideOld date
	 * @return The RideOld date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * Set The RideOld date
	 * @param date The new Date
	 */
	public void setDate(String date) {
		this.date = date;
	}
	/**
	 * Get the RideOld time
	 * @return the RideOld time
	 */
	public String getHour() {
		return hour;
	}	
	/**
	 * Set the RideOld tile
	 * @param hour The new time
	 */
	public void setHour(String hour) {
		this.hour = hour;
	}
	/**
	 * Get the event name
	 * @return The event name
	 */
	public String getNameEvent() {
		return nameEvent;
	}
	/**
	 * Set the event name
	 * @param nameEvent the new event name
	 */
	public void setNameEvent(String nameEvent) {
		this.nameEvent = nameEvent;
	}
	/**
	 * Get the event city
	 * @return The event city
	 */
	public String getCityEvent() {
		return cityEvent;
	}
	/**
	 * Set the city event 
	 * @param cityEvent The new city event
	 */
	public void setCityEvent(String cityEvent) {
		this.cityEvent = cityEvent;
	}
	/**
	 * Get the event description
	 * @return The event description
	 */
	public String getDescEvent() {
		return descEvent;
	}
	/**
	 * Set the event description
	 * @param descEvent The new description
	 */
	public void setDescEvent(String descEvent) {
		this.descEvent = descEvent;
	}
	/**
	 * return true if o is an Ride and if this.getIdRide() == o.getIdRide()
	 */
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof RideOld)
		{
			RideOld r = (RideOld)o;
			
			if(idRide != r.getIdRide())
				return false;
			
			return true;
			
		}
		else 
			return super.equals(o);
	}
	
}
