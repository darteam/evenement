package dar.pickmeup.entities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * The Event entitie
 * @author Arthur
 *
 */
public class Event {

	/**
	 * The event id, set by hibernate
	 */
	private Integer idEvent;
	/**
	 * The event name
	 */
	private String name;
	/**
	 * The event address
	 */
	private String adr;
	/**
	 * The vent placename - additional info for address
	 */
	private String placename;
	/**
	 * The city of the event
	 */
	private String city;
	/**
	 * The event departement
	 */
	private String dpt;
	/**
	 * Some tags for which define the event
	 */
	private String tag;
	/**
	 * The event description
	 */
	private String description;
	/**
	 * The event geo-coordinate 
	 */
	private String geo;
	/**
	 * The event start date
	 */
	private String dateD;
	/**
	 * The event end date
	 */
	private String dateF;
	/**
	 * An image associate to the event
	 */
	private String img;
	/**
	 * The event UID (The uid is defined by the API Evenements publics en Ile-de-France (via Open Agenda))
	 */
	private String uid;

	/**
	 * Default Constructor
	 */
	public Event()
	{
		this.name = "No Info";
		this.adr = "No Info";
		this.placename = "No Info";
		this.city = "No Info";
		this.dpt="No Info";
		this.tag = "No Info";
		this.description = "No Info";
		this.geo = "No Info";
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String current = dateFormat.format(new Date(System.currentTimeMillis()));
		
		this.dateD = current;
		this.dateF = current;
		
		this.img = "";
		this.uid = "No Info";
	}
	/**
	 * 
	 * @param name The event name
	 * @param adr The event address
	 * @param placename The event placename
	 * @param city The event city
	 * @param dpt The event departement
	 * @param tag The event tags
	 * @param description The event description
	 * @param geo The event geo coordinate
	 * @param dateD The event start date
	 * @param dateF The event end  date
	 * @param img the event image
	 * @param uid the event uid
	 */
	public Event(String name, String adr, String placename, String city,String dpt,String tag, String description, String geo, String dateD,String dateF,String img,String uid) {
		super();
		this.name = name;
		this.adr = adr;
		this.placename = placename;
		this.city = city;
		this.dpt=dpt;
		this.tag = tag;
		this.description = description;
		this.geo = geo;
		this.dateD = dateD;
		this.dateF = dateF;
		this.img = img;
		this.uid = uid;
	}

	/**
	 * Get Event Name
	 * @return the event name
	 */
	public String getName() {
		return name;
	}
	/**
	 * Set the event name
	 * @param name the new event name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Get event address
	 * @return the event address
	 */
	public String getAdr() {
		return adr;
	}
	/**
	 * Set the event address
	 * @param adr The new event address
	 */
	public void setAdr(String adr) {
		this.adr = adr;
	}
	/**
	 * Get the id event
	 * @return the event id
	 */
	public Integer getIdEvent() {
		return idEvent;
	}
	/**
	 * Set the event id !! You should not use this this is for hibernate
	 * @param idEvent 
	 */
	public void setIdEvent(Integer idEvent) {
		this.idEvent = idEvent;
	}
	/**
	 * Get the event tag
	 * @return The event event tag
	 */
	public String getTag() {
		return tag;
	}
	/**
	 * Set event tag
	 * @param tag The new tag
	 */
	public void setTag(String tag) {
		this.tag = tag;
	}
	/**
	 * get Event description
	 * @return The event description
	 */
	public String getdescription() {
		return description;
	}
	/**
	 * Set Event description
	 * @param description The new Description
	 */
	public void setdescription(String description) {
		this.description = description;
	}

	/**
	 * Get the geo coordinate
	 * @return Event geo-coordinate
	 */
	public String getGeo() {
		return geo;
	}
	/**
	 * Set the geo-coordinate
	 * @param geo the new geo-coordinate
	 */
	public void setGeo(String geo) {
		this.geo = geo;
	}
	/**
	 * Get Start date
	 * @return The event start date
	 */
	public String getDateD() {
		return dateD;
	}
	
	/**
	 * Set start date
	 * @param dateD the new start date
	 */
	public void setDateD(String dateD) {
		this.dateD = dateD;
	}
	
	/**
	 * Get the end date
	 * @return the date end
	 */
	public String getDateF() {
		return dateF;
	}
	
	/**
	 * Set the end date
	 * @param dateF the new end date
	 */
	public void setDateF(String dateF) {
		this.dateF = dateF;
	}
	
	/**
	 * Get the event place name
	 * @return The event place name
	 */
	public String getPlacename() {
		return placename;
	}
	/**
	 * Set the event place name
	 * @param placename The new event place name
	 */
	public void setPlacename(String placename) {
		this.placename = placename;
	}
	/**
	 * Get the event city
	 * @return The event city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * Set the event city
	 * @param city The event city
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * Get the event department
	 * @return The event department
	 */
	public String getDpt() {
		return dpt;
	}
	/**
	 * Set the event department
	 * @param dpt The new department
	 */
	public void setDpt(String dpt) {
		this.dpt = dpt;
	}
	/**
	 * Get the event image
	 * @return the event associate image
	 */
	public String getImg() {
		return img;
	}
	/**
	 * Set the event image
	 * @param img The new image which will be associated to the event
	 */
	public void setImg(String img) {
		this.img = img;
	}
	/**
	 * Set the Event uid - Use for the MAJ yuo should not change it
	 * @param uid The new uid
	 */
	public void setUid(String uid) {
		this.uid = uid;
	}
	/**
	 * Get the UID
	 * @return The event uid
	 */
	public String getUid() {
		return uid;
	}
	/**
	 * Return true obj is an Event and is this.getIdEvent().equals(obj.getIdEvent())
	 */
	@Override
	public boolean equals(Object obj)
	{
		if(obj instanceof Event)
		{
			Event e = (Event)obj;
			if(idEvent != e.getIdEvent())
				return false;
			
			return true;
		}
		else
			return super.equals(obj);
	}
	
}
