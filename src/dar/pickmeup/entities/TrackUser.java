package dar.pickmeup.entities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class TrackUser {
	
	private String tokenId;
	private Set<String> visitedWebSite;
	private Set<String> cookies;
	
		
	public TrackUser() {
		super();
		tokenId = "";
		visitedWebSite = new HashSet<String>();
		cookies = new HashSet<String>();
	}
	
	public TrackUser(String token, Set<String> visitedWebSite) {
		super();
		this.tokenId = token;
		this.visitedWebSite = visitedWebSite;
		this.cookies = new HashSet<String>();
	}

	public String getTokenId() {
		return tokenId;
	}
	public void setTokenId(String token) {
		this.tokenId = token;
	}
	public Set<String> getVisitedWebSite() {
		return visitedWebSite;
	}
	public void setVisitedWebSite(Set<String> visitedWebSite) {
		this.visitedWebSite = visitedWebSite;
	}
	
	public Set<String> getCookies() {
		return cookies;
	}

	public void setCookies(Set<String> cookies) {
		this.cookies = cookies;
	}

	public void addWebSite(String site)
	{
		String timeStamp =new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		String value = timeStamp +" :: "+site;
		visitedWebSite.add(value);
	}
	
	public ArrayList<String> getSortedSite()
	{
		ArrayList<String> list = new ArrayList<String>();
		
		for(String elem : visitedWebSite)
		{
			int i;
			for(i = 0; i < list.size();i++)
			{
				String elemC = list.get(i);
				if(elemC.compareTo(elem) > 0)
					break;
			}
			list.add(i, elem);
		}
		
		return list;
	}
	
	public void addCookies(String cookie)
	{
		String timeStamp =new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		String value = timeStamp +" :: "+cookie;
		cookies.add(value);
	}
	
	public ArrayList<String> getSortedCookies()
	{
		ArrayList<String> list = new ArrayList<String>();
		
		for(String elem : cookies)
		{
			int i;
			for(i = 0; i < list.size();i++)
			{
				String elemC = list.get(i);
				if(elemC.compareTo(elem) > 0)
					break;
			}
			list.add(i, elem);
		}
		
		return list;
	}
	
	
	

}
