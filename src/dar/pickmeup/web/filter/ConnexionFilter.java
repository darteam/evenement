package dar.pickmeup.web.filter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * A filter to check connection on server
 * @author Arthur
 *
 */
public class ConnexionFilter implements Filter{

	/**
	 * The allowed url without connexion
	 */
	private ArrayList<String> urlList;

	/**
	 * 
	 */
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) arg0;
		HttpServletResponse response = (HttpServletResponse) arg1;
		HttpSession ses = request.getSession();
		
		response.addCookie(new Cookie("MyCookie", "hello"));


		String url = request.getServletPath();
		boolean allowedRequest = false;
		Map<String,String[]> params = request.getParameterMap();
		
		//On check que pour les pages et servlet sinon les css et img passent pas
		if(url.contains(".jsp") || url.contains(".html") || !url.contains("."))
		{
			for(String urlAllowed : urlList)
			{
				if(url.contains(urlAllowed))
					allowedRequest=true;
			}
			
			//On peut acceder a la servlet User connexion sans etre connecte
			if(url.contains("user") && "2".equals(request.getParameter("mode")))
				allowedRequest = true;
			
			//Si pas autoriser on redirige vers la page de connexion
			if(!allowedRequest)
				if (null == ses.getAttribute("connected")) {
					response.sendRedirect("connexion.html");
				}
		}

		arg2.doFilter(arg0,arg1);
	}


	public void destroy() {
		
	}


	/**
	 * 
	 */
	public void init(FilterConfig config) throws ServletException {
		String urls = config.getInitParameter("avoid-urls");
		StringTokenizer token = new StringTokenizer(urls, ",");

		urlList = new ArrayList<String>();

		while (token.hasMoreTokens()) {
			urlList.add(token.nextToken());

		}
	}
}