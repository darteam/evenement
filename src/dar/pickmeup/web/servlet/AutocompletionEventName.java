package dar.pickmeup.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;

import dar.pickmeup.dao.EventDao;
import dar.pickmeup.entities.Event;
import dar.pickmeup.util.Tools;
/**
 * Class Servlet for auto completion in form for creation ride
 * @author Arthur
 *
 */
public class AutocompletionEventName extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private List<String> allEventsNames=null;
	private List<Event> allEvents=null;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AutocompletionEventName() {
		super();
	}

	@Override
	public void init() throws ServletException {
		allEventsNames=EventDao.getAllEventsNames();
		allEvents = dar.pickmeup.dao.EventDao.getAll();
		super.init();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO Eleminier les jar inutiles,regler le CSS,verfier l'enregistrement du trajet
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		response.setHeader("Cache-control", "no-cache, no-store");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Expires", "-1");
		
		if(!Tools.checkParams(request.getParameterMap()))
			return;
		
		String nameEvent = request.getParameter("term");
		JSONArray obj = new JSONArray();
		try{
			for (Event e : allEvents) {
				if(e.getName().contains(nameEvent)){
					JSONObject tmp = new JSONObject();
					tmp.put("label", e.getName());
					tmp.put("value", e.getName()+"-"+e.getIdEvent());
					obj.put(tmp);
				}
			}
			System.out.println(obj.toString(3));
		}catch(Exception e)
		{
			e.printStackTrace();
		}

		out.println(obj.toString());
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
