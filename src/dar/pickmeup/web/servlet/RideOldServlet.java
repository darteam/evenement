package dar.pickmeup.web.servlet;

import java.io.IOException;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;

import dar.pickmeup.dao.EventDao;
import dar.pickmeup.dao.RideDao;
import dar.pickmeup.dao.UserDao;
import dar.pickmeup.entities.Event;
import dar.pickmeup.entities.Ride;
import dar.pickmeup.entities.User;
import dar.pickmeup.services.RideOldService;
import dar.pickmeup.services.RideService;
import dar.pickmeup.util.Tools;
import dar.pickmeup.web.servlet.RideServlet.Mode;

/**
 * Servlet implementation class RideOldServlet
 * @author Arthur
 */
public class RideOldServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      /**
       * 
       * Mode for connexion
       *
       */
	public enum Mode{
		GET,
		ERROR
	}
	
	/**
	 * Get mode from int
	 * @param i
	 * @return mode
	 */
	public static Mode intToMode(int i)
	{
		switch(i)
		{
		case 0:
			return Mode.GET;
		default :
			return Mode.ERROR;
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		

		if(!Tools.checkParams(req.getParameterMap()))
			return;
		
		try{
			int mode = Integer.parseInt((String) req.getParameter("mode"));
			
			JSONObject obj= null;
			
			switch(intToMode(mode))
			{
				case GET:
					obj = getRide(req, resp);
					break;
				default :
					obj = Tools.serviceRefused("Error GET : mode incorect", 0);
					break;
			}
			
			resp.getOutputStream().write(obj.toString().getBytes());
			
		}
		catch(Exception e){
			e.printStackTrace();
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		super.doPost(req, resp);
	}
	
	/**
	 * Get ride old from http request
	 * @param req
	 * @param resp
	 * @return RideOld in JSONObject
	 * @throws Exception
	 */
	private JSONObject getRide(HttpServletRequest req, HttpServletResponse resp) throws Exception {

		String idRide_s = (String)req.getParameter("idRide");
		System.out.println("GET : " + idRide_s);
		
		if(idRide_s.compareTo("all") == 0)
		{
			JSONObject obj = RideService.getAll();
			return obj;
		}
		else
		{
			int idRide=  Integer.parseInt(idRide_s);
			JSONObject obj = RideOldService.getRideOld(idRide);
			return obj;

		}
		
		
	}
		

}
