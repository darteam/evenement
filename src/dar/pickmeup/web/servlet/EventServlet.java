package dar.pickmeup.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import dar.pickmeup.services.EventService;
import dar.pickmeup.util.Tools;
/**
 * Servelt for Event Service
 * @author Arthur
 *
 */
public class EventServlet extends HttpServlet {

	/**
	 * Connexion Mode
	 * @author Arthur
	 *
	 */
	public enum Mode{
		SEARCH,
		GET,
		ERROR
	}

	/**
	 * 
	 * Get a Mode from an int
	 * @param i 
	 * @return a mode
	 */
	public static Mode intToMode(int i)
	{
		switch(i)
		{
		case 1:
			return Mode.GET;
		default:
			return Mode.ERROR;
		}
	}

	/**
	 * 
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		super.doGet(req, resp);


		if(!Tools.checkParams(req.getParameterMap()))
			return;
		
		int mode = Integer.parseInt((String) req.getAttribute("mode"));
		JSONObject obj ;
		try{
			switch(intToMode(mode))
			{
			case GET :
				obj = getEvent(req, resp);
				break;
			default :
				obj = Tools.serviceRefused("Erreur GET : mode incorrect", 0);
				break;
			}
			resp.getOutputStream().write(obj.toString().getBytes());
		}
		catch(Exception e)
		{
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
			
		}
		
	}

	/**
	 * 
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		super.doPost(req, resp);
		resp.sendError(HttpServletResponse.SC_BAD_REQUEST);

	}

	/**
	 * Get an event from a request
	 * @param req
	 * @param resp
	 * @return An event in JSONObject 
	 * @throws Exception
	 */
	private JSONObject getEvent(HttpServletRequest req, HttpServletResponse resp) throws Exception {

		String idEvent = (String) req.getAttribute("idEvent");
		if(idEvent.compareTo("all") == 0)
			return EventService.getAll();
		else
		{
			try{
				int idE = Integer.parseInt(idEvent);
				return EventService.getEvent(idE);
			}catch(Exception e)
			{
				e.printStackTrace();
				return Tools.serviceRefused("id format inorrect", 0);
			}	
		}

	}
}
