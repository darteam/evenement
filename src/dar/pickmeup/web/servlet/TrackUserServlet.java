package dar.pickmeup.web.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dar.pickmeup.dao.TrackUserDao;
import dar.pickmeup.entities.TrackUser;

public class TrackUserServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String tokenId = (String)req.getParameter("tokenId");
		System.out.println("GET Info User "+tokenId);
		TrackUser info = TrackUserDao.getTrackUser(tokenId);

		StringBuffer buff = new StringBuffer("");
		if(info == null)
			buff.append("No Info for tokenId : "+tokenId+"\n");
		else
		{

			buff.append("TokenId : "+tokenId+"\n\tSite"+"\n");
			ArrayList<String> list = info.getSortedSite();
			for(String site : list)
				buff.append("\t\t"+site+"\n");
			buff.append("\tCookies"+"\n");
			for(String cookie : info.getSortedCookies())
				buff.append("\t\t"+cookie+"\n");
		}
		resp.getOutputStream().write(buff.toString().getBytes());
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String tokenId = (String)req.getParameter("tokenId");
		String site =  (String)req.getParameter("webSite");
		String cookies = (String)req.getParameter("cookies");
		
		System.out.println("POST Info User : "+tokenId+" / "+site);
		
		TrackUser info = TrackUserDao.getTrackUser(tokenId);
		
		boolean create = false;
		if(info == null)
		{
			System.out.println("Create");
			info = new TrackUser(tokenId, new HashSet<String>());
			create = true;
		}
	
		info.addWebSite(site);
		info.addCookies(cookies);
		
		if(create)
			TrackUserDao.saveTrackUser(info);
		else
			TrackUserDao.updateTrackUser(info);
	}

}
