package dar.pickmeup.web.servlet;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dar.pickmeup.entities.User;
import dar.pickmeup.util.Tools;

/**
 * Servlet implementation class CreateUserServlet
 */
public class CreateUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			Map<String,String[]> map=request.getParameterMap();

			if(!Tools.checkParams(request.getParameterMap()))
				return;
			
			response.setContentType("text/plain");
			
			if(   	map.containsKey("email")     && !request.getParameter("email").equals("")
					&&map.containsKey("password")     && !request.getParameter ("password").equals("")
					&&map.containsKey("firstName")	 && !request.getParameter ("firstName").equals("")
					&&map.containsKey("lastName")  && !request.getParameter ("lastName").equals("")
					&&map.containsKey("confirmation") && !request.getParameter("confirmation").equals("")){

				if (request.getParameter("password").equals(request.getParameter("confirmation"))) 
				{
					User user = new User(request.getParameter("email"), request.getParameter("password"), request.getParameter("lastName"), request.getParameter("firstname"));					
					dar.pickmeup.services.UserService.createUser(user);
					response.sendRedirect("connexion.html");
				} else {
					response.getWriter().print(
						Tools.serviceRefused("Le mot de passe et sa confirmation ne sont pas similaires !",0));
				}
			}else throw new Exception("Wrong Url! Missing parameters\n Il manque des parametres  l'URL!");

		}catch (Exception e) {
			e.printStackTrace(); //local debug
			request.setAttribute("error", e.getMessage()); //remote debug
			request.getRequestDispatcher("errorpage.jsp").forward(request, response);}
	}

}
