package dar.pickmeup.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import dar.pickmeup.dao.RideDao;
import dar.pickmeup.entities.Event;
import dar.pickmeup.entities.Ride;
import dar.pickmeup.util.Tools;

/**
 * Servlet implementation class SearchRide
 */
public class SearchRide extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchRide() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		if(!Tools.checkParams(request.getParameterMap()))
			return;
		PrintWriter out = response.getWriter();
        response.setContentType("application/json");
		String adresse = request.getParameter("city");
		String evenName = request.getParameter("inputEvent");
		String idUser = (String) request.getSession().getAttribute("user");
		
		List<Ride> rides=RideDao.getRideByEventName(evenName,adresse,idUser);
		JSONObject obj =new JSONObject();
		
		try{
			
			JSONArray array = new JSONArray();
			for(Ride r : rides)
			{
				JSONObject ride = Tools.serviceMessage(r);
				JSONArray lPas = ride.getJSONArray("pas");
				JSONArray lApp = ride.getJSONArray("app");
				
				if(lPas.length() == 0)
					ride.put("isOn", false);
				else
				{
					boolean isOn = false;
					for(int i = 0;i<lPas.length();i++)
					{
						if(lPas.get(i).equals(idUser))
							isOn=true;
					}
					ride.put("isOn", isOn);
				}
				
				if(lApp.length() == 0)
					ride.put("isAppli", false);
				else
				{
					boolean isAppli = false;
					for(int i = 0;i<lApp.length();i++)
					{
						if(lApp.get(i).equals(idUser))
							isAppli=true;
					}
					ride.put("isAppli", isAppli);
				}
			
				array.put(ride);
			}
			obj.put("data", array);
			System.out.println(obj.toString(3));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	
		out.println(obj.toString());
        out.close();
	}

}
