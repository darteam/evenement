package dar.pickmeup.web.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;


import dar.pickmeup.dao.EventDao;
import dar.pickmeup.dao.RideDao;
import dar.pickmeup.dao.UserDao;
import dar.pickmeup.entities.Event;
import dar.pickmeup.entities.Ride;
import dar.pickmeup.entities.User;
import dar.pickmeup.services.RideService;
import dar.pickmeup.services.UserService;
import dar.pickmeup.util.Tools;
/**
 * Servlet for Ride ressource
 * @author Arthur
 *
 */
public class RideServlet extends HttpServlet {

	/**
	 * Mode for connexion
	 * @author Arthur
	 */
	public enum Mode{
		CREATE,
		UPDATE_ADD,
		UPDATE_REMOVE,
		UPDATE_ADD_A,
		UPDATE_REMOVE_A,
		UPDATE_DATE,
		UPDATE_NBMAX,
		UPDATE_TIME,
		GET,
		ERROR
	}
	
	/**
	 * Get a mode from an int
	 * @param i
	 * @return mode
	 */
	public static Mode intToMode(int i)
	{
		switch(i)
		{
		case 0:
			return Mode.CREATE;
		case 1:
			return Mode.UPDATE_ADD;
		case 2:
			return Mode.UPDATE_REMOVE;
		case 3:
			return Mode.UPDATE_ADD_A;
		case 4:
			return Mode.UPDATE_REMOVE_A;
		case 5:
			return Mode.UPDATE_DATE;
		case 6 :
			return Mode.UPDATE_NBMAX;
		case 7:
			return Mode.UPDATE_TIME;
		case 8:
			return Mode.GET;
		default :
			return Mode.ERROR;
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		

		if(!Tools.checkParams(req.getParameterMap()))
			return;
		try{
			int mode = Integer.parseInt((String) req.getParameter("mode"));
			resp.setContentType("application/json");
			JSONObject obj= null;
			
			switch( RideServlet.intToMode(mode))
			{
				case GET:
					obj = getRide(req, resp);
					break;
				default :
					obj = Tools.serviceRefused("Error GET : mode incorect", 0);
					break;
			}
			
			resp.getOutputStream().write(obj.toString().getBytes());
			
		}
		catch(Exception e){
			e.printStackTrace();
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		

		if(!Tools.checkParams(req.getParameterMap()))
			return;
		try{
			
			resp.setContentType("application/json");
			int mode = Integer.parseInt((String) req.getParameter("mode"));
			JSONObject obj;
			System.out.println("Mode :: "+mode);
			switch(RideServlet.intToMode(mode))
			{
				case CREATE :
					obj = createRide(req, resp);
					break;
				case UPDATE_ADD:
					obj = addPassenger(req, resp);
					break;
				case UPDATE_REMOVE:
					obj = rmPassenger(req, resp);
					break;
				case UPDATE_ADD_A:
					obj = addApplicant(req, resp);
					break;
				case UPDATE_REMOVE_A:
					obj = rmApplicant(req, resp);
					break;
				case UPDATE_DATE:
					obj = setDate(req, resp);
					break;
				case UPDATE_TIME:
					obj = setTime(req, resp);
					break;
				case UPDATE_NBMAX:
					obj = setNBMax(req, resp);
					break;
			
				default :
					obj = Tools.serviceRefused("Error  POST: mode incorect - "+mode, 0);
					break;
			}
			resp.getOutputStream().write(obj.toString().getBytes());

			
		}
		catch(Exception e){
			e.printStackTrace();
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
		}
		
	}
	
	/**
	 * Get ride from a resquest http
	 * @param req
	 * @param resp
	 * @return A ride in JSON
	 * @throws Exception
	 */
	private JSONObject getRide(HttpServletRequest req, HttpServletResponse resp) throws Exception {

		
		HttpSession ses = req.getSession();
		String idUser =(String) ses.getAttribute("user");
		String idRide_s = (String)req.getParameter("idRide");
		System.out.println("GET : " + idRide_s);
		
		//On renvoie tous les rides
		if(idRide_s.compareTo("all") == 0)
		{
			JSONObject obj = RideService.getAll();
			JSONArray arr = obj.getJSONArray("data");
			for(int i = 0; i<arr.length();i++)
			{
				
				obj.getJSONObject("data").put("owner",false);
				obj.getJSONObject("data").remove("idsApplicants");
			}
			return obj;
		}
		else
		{
			//On ajoute a l'objet JSON des info sur l'utilisateur
			//Facilite le traitement en js
			//isOwner -> l'utilisateur qui fait la requete est il le proprietaire du trajet
			//isPAs -> L'uitlisateut est il un passager
			//isApp -> Lutiisateur a t'il une demain en attente sur ce trajet
			
			int idRide=  Integer.parseInt(idRide_s);
			JSONObject obj = RideService.getRide(idRide);
			String rideUser = obj.getString("owner");
			System.out.println(" ===> "+rideUser);
			boolean owner = rideUser.compareTo(idUser) == 0;
			obj.put("isOwner",owner);
			if(!owner)			
			{
				
				boolean isOn = false;
				boolean isAppli = false;
				
				JSONArray array = obj.getJSONArray("pas");
				for(int i = 0 ; i < array.length();i++)
				{
					if(array.getString(i).equals(idUser))
						isOn = true;						
				}
				array = obj.getJSONArray("app");
				for(int i = 0 ; i < array.length();i++)
				{
					if(array.getString(i).equals(idUser))
						isAppli = true;						
				}
				
				obj.remove("app");
				obj.put("isOn",isOn);
				obj.put("isAppli",isAppli);
			}
			System.out.println(obj.toString(3));
			return obj;

		}
		
		
	}
	
	/**
	 * Create ride from  http request 
	 * @param req
	 * @param resp
	 * @return JSON object
	 * @throws Exception
	 */
	private JSONObject createRide(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		
		HttpSession ses = req.getSession();
		
		String idUser = (String)ses.getAttribute("user");
		String nameEvent=(String) req.getParameter("nameEvent");
		
		//On recup l'id de l'event dans le nom
		StringTokenizer tk =new StringTokenizer(nameEvent, "-");
		while(tk.hasMoreTokens())
			nameEvent = tk.nextToken();
		System.out.println(nameEvent);
		
		
		int idEvent = Integer.parseInt(nameEvent);
		int nbMaxP = (Integer)Integer.parseInt(req.getParameter("nbMaxP"));
		String ville = (String)req.getParameter("city");
		String date = (String)req.getParameter("date");
		String time = (String)req.getParameter("time");
		User owner = UserDao.getUser(idUser);
		
		Ride r = new Ride(EventDao.getEvent(idEvent),owner,nbMaxP,ville,date,time);

		JSONObject obj =  RideService.addRide(r);

		owner.addRide(r);
		UserDao.updateUser(owner);
		
		resp.sendRedirect("ride.html?idRide="+r.getIdRide());
		return obj;
	}
	
	/**
	 * Add a pasengers in a ride
	 * @param req
	 * @param resp
	 * @return JSon Object
	 * @throws Exception
	 */
	private JSONObject addPassenger(HttpServletRequest req, HttpServletResponse resp) throws Exception
	{
		HttpSession ses = req.getSession();
		String idUser =(String)ses.getAttribute("user");
		int idRide = Integer.parseInt((String) req.getParameter("idRide"));
		String idPas = (String) req.getParameter("idPas");

		Ride r = RideDao.getRide(idRide);
		User u = null;
		
		for(User tmp : r.getApplicants())
			if(tmp.getEmail().equals(idPas))
				u = tmp;
		String notif = idRide+"-Vous etes sur "+idRide;
		
		//Il n'y a que la proprio qui peut ajouter un passager
		if(r.getOwner().getEmail().equals(idUser))
		{
			u.addRide(r);
	        u.getRidesWait().remove(r);
	        u.getRides().add(r);
	        u.addNotif(notif);
			UserDao.updateUser(u);
			
			return RideService.update(r);
		}
		else
			return Tools.serviceRefused("Vous n'etes pas authoriser a rajouter un passager", 0);
	}
	
	/**
	 * Delete a passenger from a ride
	 * @param req
	 * @param resp
	 * @return
	 * @throws Exception
	 */
	private JSONObject rmPassenger(HttpServletRequest req, HttpServletResponse resp) throws Exception
	{
		HttpSession ses = req.getSession();
		String idUser =   (String)ses.getAttribute("user");
		int idRide = Integer.parseInt((String) req.getParameter("idRide"));
		
		

		Ride r = RideDao.getRide(idRide);
		User owner = r.getOwner();
		User u = null;
		
		
		//Si c'est une demande du proprio on recup l'id du passager
		//puis on supprime le passger
		if(owner.getEmail().equals(idUser))
		{
			String idPas = (String) req.getParameter("idPas");
		
			for(User tmp : r.getPassengers())
				if(tmp.getEmail().equals(idPas))
					u = tmp;
			String notif = idRide+"-Suppr du Trajet"+idRide;
			
			u.removeRide(r);
			u.addNotif(notif);
			UserDao.updateUser(u);
			return RideService.update(r);
		}
		else // Sinon c'est une demande du passger on le cherche et on le suppr
		{
			
			for(User tmp : r.getPassengers())
				if(tmp.getEmail().equals(idUser))
					u = tmp;
			
			if(r.getPassengers().contains(u))
			{
				System.out.println("Remove Usuer");
				u.removeRide(r);
				UserDao.updateUser(u);
				return RideService.update(r);
			}
		}
		
		return Tools.serviceRefused("Vous n'etes pas authoriser a enlever un passager", 0);
	}
	
	/**
	 * Add an applicant
	 * @param req
	 * @param resp
	 * @return
	 * @throws Exception
	 */
	private JSONObject addApplicant(HttpServletRequest req, HttpServletResponse resp) throws Exception
	{
		HttpSession ses = req.getSession();
		String idUser =(String)ses.getAttribute("user");
		int idRide = Integer.parseInt((String) req.getParameter("idRide"));
		
		Ride r = RideDao.getRide(idRide);
		User owner = r.getOwner();
		User u = UserDao.getUser(idUser);
		owner.addNotif(r.getIdRide()+"-Nouvelle demande");
		UserDao.updateUser(owner);
		
		if(owner.getEmail().equals(idUser))
		{
			return Tools.serviceRefused("Vous n'etes pas authoriser a ajouter une demande", 0);
		}
		else if(!r.getApplicants().contains(u))
		{
			u.addWaited(r);
			UserDao.updateUser(u);
			
			return RideService.update(r);
		}
		
		
		return Tools.serviceRefused("Vous n'etes pas authoriser a ajouter une demande", 0);
	}
	
	/**
	 * Remove and Applicant from a ride
	 * @param req
	 * @param resp
	 * @return
	 * @throws Exception
	 */
	private JSONObject rmApplicant(HttpServletRequest req, HttpServletResponse resp) throws Exception
	{
		HttpSession ses = req.getSession();
		String idUser = (String)ses.getAttribute("user");
		int idRide = Integer.parseInt((String) req.getParameter("idRide"));
		

		Ride r = RideDao.getRide(idRide);
		User owner = r.getOwner();
		User u = null;
		
		String notif = idRide+"- Demande Suppr "+idRide;
		
				
		if(owner.getEmail().equals(idUser))
		{
			String idPas = (String) req.getParameter("idPas");
			
			for(User tmp : r.getApplicants())
				if(tmp.getEmail().equals(idPas))
					u = tmp;
			u.addNotif(notif);
			u.removeRideWait(r);
			UserDao.updateUser(u);
			
			return RideService.update(r);
		}
		else if(r.getApplicants().contains(u)) 
		{
			for(User tmp : r.getApplicants())
				if(tmp.getEmail().equals(idUser))
					u = tmp;
			r.removeApplicants(u);
			u.removeRideWait(r);
			UserDao.updateUser(u);
			return RideService.update(r);
		}
		
		return Tools.serviceRefused("Vous n'etes pas authoriser a enlever un passager", 0);
	}
	
	/**
	 * Change the date of a ride
	 * @param req
	 * @param resp
	 * @return
	 * @throws Exception
	 */
	private JSONObject setDate(HttpServletRequest req, HttpServletResponse resp) throws Exception
	{
		HttpSession ses = req.getSession();
		String idUser = (String)ses.getAttribute("user");
		int idRide = Integer.parseInt((String) req.getParameter("idRide"));
		String date = (String) req.getParameter("date");

		Ride r = RideDao.getRide(idRide);
		User owner = r.getOwner();
		
		String notif = r.getIdRide()+"-La date du trajet "+idRide+" a change";
		for(User u : r.getPassengers())
		{
			u.addNotif(notif);
			UserDao.updateUser(u);
		}
		
		if(owner.getEmail().equals(idUser))
		{
			if(!checkDateFormat(date))
				return Tools.serviceRefused("Erreur format date == YYYY-MM-DD", 0);
			if(!checkDate(r.getEvent(), date))
				return Tools.serviceRefused("Erreur date non conforme a l'evenement", 0);
			
			r.setDate(date);
			return RideService.update(r);
		}
		else
			return Tools.serviceRefused("Vous n'etes pas authoriser a changer la date", 0);
	}

	/**
	 * Change nb max passengers in a ride
	 * @param req
	 * @param resp
	 * @return
	 * @throws Exception
	 */
	private JSONObject setNBMax(HttpServletRequest req, HttpServletResponse resp) throws Exception
	{
		HttpSession ses = req.getSession();
		String idUser = (String)ses.getAttribute("user");
		int idRide = Integer.parseInt((String) req.getParameter("idRide"));
		int nbMax  = Integer.parseInt((String) req.getParameter("nbMax"));

		Ride r = RideDao.getRide(idRide);
		User owner = r.getOwner();
		
		if(owner.equals(idUser))
		{
			if(nbMax > 0)
			{
				r.setMaxNbPassengers(nbMax);
				return RideService.update(r);	
			}
			else
				return Tools.serviceRefused("Nombre max de passagers incorrect", 0);
			
		}
		else
			return Tools.serviceRefused("Vous n'est pas autoriser a changer le nombre de passager", 0);
	}
	
	/**
	 * Change ride time
	 * @param req
	 * @param resp
	 * @return
	 * @throws Exception
	 */
	private JSONObject setTime(HttpServletRequest req, HttpServletResponse resp) throws Exception
	{
		HttpSession ses = req.getSession();
		String idUser = (String)ses.getAttribute("user");
		int idRide = Integer.parseInt((String) req.getParameter("idRide"));
		String time  = (String) req.getParameter("time");

		Ride r = RideDao.getRide(idRide);
		User owner = r.getOwner();
		
		String notif = r.getIdRide()+"-L'heure du trajet a change";
		
		for(User u : r.getPassengers())
		{
			u.addNotif(notif);
			UserDao.updateUser(u);
		}
		
		if(owner.getEmail().equals(idUser))
		{
			if(checkTime(time))
			{
				r.setTime(time);
				return RideService.update(r);	
			}
			else
				return Tools.serviceRefused("Nombre max de passagers incorrect", 0);
			
		}
		else
			return Tools.serviceRefused("Vous n'est pas autoriser a changer le nombre de passager", 0);
		
	
	}

	/**
	 * Check if a date as the good format (YYYY-MM-DD)
 	 * @param date
	 * @return true if it's good false other wise
	 */
	private boolean checkDateFormat(String date)
	{
		try
		{
			StringTokenizer tk = new StringTokenizer(date,"-");
			
			int year = Integer.parseInt(tk.nextToken());
			int month = Integer.parseInt(tk.nextToken());
			int day = Integer.parseInt(tk.nextToken());
			
			if(month <= 12 && month > 0)
			{
				if(day>0 && day <31)
				{
					return true;
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	
		return false;
	}

	/**
	 * Check if a date is good for an event - date between date start and date end
	 * @param e
	 * @param date
	 * @return true if it's good false otherwise
	 */
	private boolean checkDate(Event e,String date)
	{
		String dateD = e.getDateD();
		String dateF = e.getDateF();
		if(dateD.compareTo(date) <= 0 && dateF.compareTo(date) >= 0)
			return true;
		return false;
	}
	
	/**
	 * Check if the time exist 00:00 -> 23:59
	 * @param date
	 * @return  true if it's good false otherwise
	 */
	private boolean checkTime(String date)
	{
		try
		{
			StringTokenizer tk = new StringTokenizer(date,":");
			
			int hh = Integer.parseInt(tk.nextToken());
			int mm = Integer.parseInt(tk.nextToken());
			
			if(hh <= 23 && hh >= 0)
			{
				if(mm>=0 && mm <60)
				{
					return true;
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	
		return false;
	}

}
