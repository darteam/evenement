package dar.pickmeup.web.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import dar.pickmeup.dao.EventDao;
import dar.pickmeup.dao.RideDao;
import dar.pickmeup.dao.UserDao;
import dar.pickmeup.entities.Event;
import dar.pickmeup.entities.Ride;
import dar.pickmeup.entities.User;
import dar.pickmeup.services.UserService;
import dar.pickmeup.util.Tools;
import sun.rmi.transport.proxy.HttpReceiveSocket;
/**
 * Servlet for User ressource
 * @author Arthur
 */

public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Mode for connexion
	 * @author Arthur
	 *
	 */
	public enum Mode {
		INFO,
		GET,
		CONNEXION,
		CREATE,
		NOTIF,
		DECO,
		ERROR,
	}

	public UserServlet() {
		super();
	}

	/**
	 * Change int to mode
	 * @param i
	 * @return mode
	 */
	public Mode intToMode(int i)
	{
		switch(i)
		{
		case 0 :
			return Mode.INFO;
		case 1 :
			return Mode.GET;
		case 2 :
			return Mode.CONNEXION;
		case 3:
			return Mode.CREATE;
		case 4:
			return Mode.NOTIF;
		case 5 :
			return Mode.DECO;
		default:
			return Mode.ERROR;
		}
	}

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		if(!Tools.checkParams(request.getParameterMap()))
			return;
		if(!request.getParameterMap().containsKey("mode"))
		{
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
		}
		else
		{
			int int_mode = Integer.parseInt((String)request.getParameter("mode"));
			switch(intToMode(int_mode))
			{
			case GET:
				getUser(request,response);
				break;
			case DECO:
				deco(request,response);
				break;
			case ERROR :
			default:
				response.sendError(HttpServletResponse.SC_BAD_REQUEST);
				break;
			}

		}	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		if(!Tools.checkParams(request.getParameterMap()))
			return;
		if(!request.getParameterMap().containsKey("mode"))
		{
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
		}
		else
		{
			try{
				int int_mode = Integer.parseInt((String)request.getParameter("mode"));
				switch(intToMode(int_mode))
				{
				case CONNEXION:
					connexion(request,response);
					break;
				case CREATE:
					response.getOutputStream().write(Tools.serviceRefused("Not Implemented",0).toString().getBytes());
					break;
				case NOTIF:
					rmNotif(request, response);
					break;
				default:
					throw new Exception();
				}
			}
			catch(Exception e)
			{
				response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			}

		}	
	}

	/**
	 * Get User from http request
	 * @param req
	 * @param resp
	 * @throws ServletException
	 * @throws IOException
	 */
	private void getUser(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {

		try{
			HttpSession ses = req.getSession();
			String idUser = (String)ses.getAttribute("user");

			JSONObject user = UserService.getUserInfo(idUser);
			resp.getOutputStream().write(user.toString().getBytes());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
		}
	}
	
	/**
	 * Handle a connexion from request http
	 * @param req
	 * @param resp
	 * @throws ServletException
	 * @throws IOException
	 */
	public void connexion(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession ses = req.getSession();
		String login = (String)req.getParameter("login");
		String pwd = (String)req.getParameter("pwd");
		System.out.println("PARAM ok - "+login+" -- "+pwd);
		User u = new User();
		u.setEmail(login);
		u.setPassword(pwd);
		JSONObject obj = UserService.checkUserConnexion(u);
		
		try {
			
			
			if(obj.getString("state").equals("SUCCESS"))
			{
				ses.setAttribute("user", login);
				ses.setAttribute("connected", true);
				resp.getOutputStream().write(obj.toString().getBytes());
				resp.sendRedirect("mainpage.html");
			}
			else
				resp.getOutputStream().write("<a href=\"connexion.html\">Connexion refusee</a>".getBytes());	
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	/**
	 * Remove notif for a user
	 * @param req
	 * @param resp
	 * @throws Exception
	 */
	private void rmNotif(HttpServletRequest req, HttpServletResponse resp) throws Exception
	{
		System.out.println("RM NOTIF");
		HttpSession ses = req.getSession();
		String idUser = (String) ses.getAttribute("user");
		String idRide = (String) req.getParameter("idRide");

		User u = UserDao.getUser(idUser);
		
		u.removeNotif(idRide);
		UserDao.updateUser(u);
		
		JSONObject obj = new JSONObject();
		
		if(RideDao.rideExist(Integer.parseInt(idRide)))
		{
			obj.put("redirect", "true");
			obj.put("idRide", idRide);
			resp.getWriter().write(obj.toString());
		}
		else
		{
			obj.put("redirect", "true");
			obj.put("idRide", idRide);
			obj.put("user", Tools.serviceMessage(u));
			resp.getWriter().write(obj.toString());
		}
		
	}
	
	/**
	 * Deconnexion function remove session attribute
	 * @param req
	 * @param resp
	 * @throws IOException
	 */
	public void deco(HttpServletRequest req, HttpServletResponse resp) throws IOException
	{
		HttpSession ses = req.getSession();
		ses.removeAttribute("connected");
		ses.removeAttribute("user");
		resp.getWriter().write("{}");
	}
}
