package dar.pickmeup.test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import org.json.JSONObject;

import dar.pickmeup.dao.EventDao;
import dar.pickmeup.dao.RideDao;
import dar.pickmeup.dao.UserDao;
import dar.pickmeup.entities.Event;
import dar.pickmeup.entities.Ride;
import dar.pickmeup.entities.RideOld;
import dar.pickmeup.entities.User;
import dar.pickmeup.services.EventService;
import dar.pickmeup.util.BDDUpdate;
import dar.pickmeup.util.ExternAPIConnexion;
import dar.pickmeup.util.Tools;

public class TestUpdateBdd {


	public static void main(String args[]) throws Exception
	{
		BDDUpdate.init();

		TestBDD.initBD();
		BDDUpdate.update();

		Calendar date = Calendar.getInstance();
		date.set(Calendar.MINUTE,date.get(Calendar.MINUTE)+1);
		BDDUpdate.getSingleton().keepUpdate(date, 1000*60*5);
	}

}
