package dar.pickmeup.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import dar.pickmeup.entities.Event;
import dar.pickmeup.entities.Ride;
import dar.pickmeup.entities.User;

public class TestRide {

	private Ride r1;
	private Ride r2;
	private Ride r3;
	private Ride r4;
	
	private User u1;
	private User u2;
	private User u3;
	
	private Event e1;
	private Event e2;
	private Event e3;
	
	@Before
	public void init()
	{
		try{
			
			u1 = new User("ar@joanny.com", "12345", "Joanny", "Arthur");
			u2 = new User("wa@cheglal.com", "12345", "Cheglal", "Wafae");
			u3 = new User("ay@sarih.com", "12345", "Sarih", "Ayoub");
			
			Event e1 = new Event("Test #1","Jussieu", "UPMC", "Paris", "75", "Test", "Test", "Geo", "2016-01-01", "2016-01-01", "img", "12345");
			Event e2 = new Event("Test #2","Jussieu", "UPMC", "Paris", "75", "Test", "Test", "Geo", "2016-01-01", "2016-01-01", "img", "12345");
			Event e3 = new Event("Test #3","Jussieu", "UPMC", "Paris", "75", "Test", "Test", "Geo", "2016-01-01", "2016-01-01", "img", "12345");
			
			e1.setIdEvent(1);
			e2.setIdEvent(2);
			e3.setIdEvent(3);
			
			r1 = new Ride(e1,u1,5, "Paris", "2016-12-25","12:00");
			r1.addPassenger(u2);
			r1.setIdRide(1);
			
			r2 = new Ride(e2,u2,5, "Paris", "2016-12-25","12:00");
			r2.addPassenger(u1);
			r2.addPassenger(u3);
			r2.setIdRide(2);
			
			r3 = new Ride(e3,u3,5, "Paris", "2016-12-25","12:00");
			r3.addApplicants(u1);
			r3.addApplicants(u2);
			r3.setIdRide(3);
			
			r4 = new Ride(e1,u1,5, "Paris", "2016-12-25","12:00");
			r4.addPassenger(u2);
			r4.setIdRide(1);
		}
		catch(Exception e)
		{
			fail();
		}
	
	}
	
	@Test
	public void testEquals() {
		assertTrue(r1.equals(r4));
		assertFalse(r1.equals(r3));
	}
	
	@Test
	public void testAddPassenger()
	{
		try{
			r1.addPassenger(u3);
		}catch(Exception e)
		{
			fail();
		}
		assertTrue(r1.getPassengers().size() == 2);
		assertTrue(r1.getPassengers().contains(u2));
		assertTrue(r1.getPassengers().contains(u3));
	
	}
	
	@Test
	public void testRemovePassenger()
	{

		try{
			r1.removePassenger(u2);
			
		}catch(Exception e)
		{
			fail();
		}
		assertTrue(r1.getPassengers().size() == 0);
	}
	
	@Test
	public void testAddApplicants()
	{
		try{
			r1.addApplicants(u3);
			
		}catch(Exception e)
		{
			fail();
		}
		assertTrue(r1.getApplicants().size() == 1);
		assertTrue(r1.getApplicants().contains(u3));
	
	}
	
	@Test
	public void testRemoveApplicants()
	{

		try{
			r3.removeApplicants(u1);
			
		}catch(Exception e)
		{
			fail();
		}
		assertTrue(r3.getApplicants().size() == 1);
		assertTrue(r3.getApplicants().contains(u2));
	
	}
}
