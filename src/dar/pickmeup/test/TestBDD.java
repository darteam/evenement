package dar.pickmeup.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import dar.pickmeup.dao.EventDao;
import dar.pickmeup.dao.RideDao;
import dar.pickmeup.dao.UserDao;
import dar.pickmeup.entities.Event;
import dar.pickmeup.entities.Ride;
import dar.pickmeup.entities.User;
import dar.pickmeup.util.BDDUpdate;

public class TestBDD {

	public static void main (String args[])
	{
	
		
		try {
		initBD();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void initBD() throws Exception
	{
		User u1 = new User("ay@sarih.com", "12345", "Sarih", "Ayoub");
		User u2 = new User("wa@cheglal.com", "12345", "Cheglal", "Wafae");
		User u3 = new User("ar@joanny.com", "12345", "Joanny", "Arthur");
		User u4 = new User("user@test.com", "12345", "User", "Test");
		
		
		u1.addNotif("5-Notif 1");
		u1.addNotif("5-Notif 2");
		u1.addNotif("6-Notif 3");
		
		u2.addNotif("5-Notif 1");
		u2.addNotif("5-Notif 2");
		
		u3.addNotif("5-Notif 1");
		u3.addNotif("5-Notif 2");
		
		System.out.println("Add User");
		UserDao.addUser(u1);
		UserDao.addUser(u2);
		UserDao.addUser(u3);
		UserDao.addUser(u4);
		
		Event e1 = new Event("Test #1", "Universite UPMC", "Tour 14-15 - 3eme", "Paris", "75", "Univ Info", "programme", "geo", "2016-10-30","2016-10-30", "", "1");
		e1.setdescription("Conference sur un truc super interessant qui n'est pas obligatoire et ou personne ne vas venir\n");
		Event e2 = new Event("Test #2", "Universite UPMC", "Tour 14-15 - 3eme", "Paris", "75", "Univ Math", "statistique", "geo", "2016-10-30","2016-10-30", "", "2");
		e2.setdescription("Cette description n'est pas utile c'est juste un test et je dois la remplir");
		Event e3 = new Event("Test #3", "Universite UPMC", "Tour 14-15 - 3eme", "Paris", "75", "Math", "probabilite", "geo", "2016-10-30","2016-11-25", "", "3");
		e3.setdescription("Probabilite statistique cours de soutient de math\n ");
		Event e4 = new Event("Test #4", "Universite UPMC", "Tour 14-15 - 3eme", "Paris", "75", "Info", "web", "geo", "2016-12-31","2016-12-31", "", "4");
		e4.setdescription("Une autre description juste pour tester la mise en page\nAlors on saute des lignes et on ecrit pour rien\nSalut a tous");
		
		
		System.out.println("Add Event");
		EventDao.addEvent(e1);
		EventDao.addEvent(e2);
		EventDao.addEvent(e3);
		EventDao.addEvent(e4);
		
		
		Ride r1 = new Ride(e1,u1,4,"Paris");
		r1.addPassenger(u2);
		Ride r2 = new Ride(e2,u2,4,"Paris");
		r2.addPassenger(u3);
		Ride r3 = new Ride(e3,u3,4,"Paris");
		r3.addPassenger(u1);
		Ride r4 = new Ride(e4,u1,4,"Paris");
		r4.addPassenger(u2);
		r4.addPassenger(u3);
		Ride r5 = new Ride(e1,u2,4,"Paris");
		r5.addPassenger(u3);
		r5.addPassenger(u1);
		Ride r6 = new Ride(e2,u3,4,"Paris");
		r6.addPassenger(u1);
		r6.addPassenger(u2);
		Ride r7 = new Ride(e3,u1,4,"Paris");
		r7.addPassenger(u2);
		r7.addPassenger(u3);
		Ride r8 = new Ride(e4,u2,4,"Paris");
		r8.addApplicants(u3);
		Ride r9 = new Ride(e1,u3,4,"Paris");
		r9.addApplicants(u1);
		Ride r10 = new Ride(e2,u1,4,"Paris");
		r10.addApplicants(u2);
		Ride r11 = new Ride(e3,u2,4,"Paris");
		r11.addApplicants(u3);
		Ride r12 = new Ride(e4,u3,4,"Paris");
		r12.addApplicants(u1);
		r12.addApplicants(u2);
		Ride r13 = new Ride(e1,u1,4,"Paris");
		r13.addApplicants(u2);
		r13.addApplicants(u3);
		Ride r14 = new Ride(e2,u2,4,"Paris");
		r14.addApplicants(u1);
		r14.addApplicants(u3);
		
		
		ArrayList<Ride> rides = new ArrayList<Ride>();
		rides.add(r1);
		rides.add(r2);
		rides.add(r3);
		rides.add(r4);
		rides.add(r5);
		rides.add(r6);
		rides.add(r7);
		rides.add(r8);
		rides.add(r9);
		rides.add(r10);
		rides.add(r11);
		rides.add(r12);
		rides.add(r13);
		rides.add(r14);
		
	
		List<Ride> listRides = RideDao.getAll();
		for(Ride r : listRides)
		{
			if	(r.getOwner().equals(u1))
				u1.addRide(r);
			else if(r.getOwner().equals(u2))
				u2.addRide(r);
			else
				u3.addRide(r);
			
			if(r.getPassengers().contains(u1))
				u1.addRide(r);
			if(r.getPassengers().contains(u2))
				u2.addRide(r);
			if(r.getPassengers().contains(u3))
				u3.addRide(r);
			
			if(r.getApplicants().contains(u1))
				u1.addWaited(r);
			if(r.getApplicants().contains(u2))
				u2.addWaited(r);
			if(r.getApplicants().contains(u3))
				u3.addWaited(r);
		}
		
		System.out.println("Add Ride");
		for(Ride r : rides)
			RideDao.addRide(r);

		
		UserDao.updateUser(u1);
		UserDao.updateUser(u2);
		UserDao.updateUser(u3);
		
	
		

	}
}
