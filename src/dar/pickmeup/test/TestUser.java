package dar.pickmeup.test;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

import dar.pickmeup.entities.Event;
import dar.pickmeup.entities.Ride;
import dar.pickmeup.entities.RideOld;
import dar.pickmeup.entities.User;

public class TestUser {

	private Ride r1;
	private RideOld ro1;
	
	private User u1;
	private User u2;
	private User u3;
	private User u4;
	
	private Event e1;
	private Event e2;
	private Event e3;
	
	@Before
	public void init()
	{
		try{
		u1 = new User("ar@joanny.com", "12345", "Joanny", "Arthur");
		u2 = new User("wa@cheglal.com", "12345", "Cheglal", "Wafae");
		u3 = new User("ay@sarih.com", "12345", "Sarih", "Ayoub");
		u4 =  new User("ar@joanny.com", "12345", "Joanny", "Arthur");
		
		Event e1 = new Event("Test #1","Jussieu", "UPMC", "Paris", "75", "Test", "Test", "Geo", "2016-01-01", "2016-01-01", "img", "12345");
		Event e2 = new Event("Test #2","Jussieu", "UPMC", "Paris", "75", "Test", "Test", "Geo", "2016-01-01", "2016-01-01", "img", "12345");
		Event e3 = new Event("Test #3","Jussieu", "UPMC", "Paris", "75", "Test", "Test", "Geo", "2016-01-01", "2016-01-01", "img", "12345");
		
		e1.setIdEvent(1);
		e2.setIdEvent(2);
		e3.setIdEvent(3);
		
		r1 = new Ride(e1,u1,5, "Paris", "2016-12-25","12:00");
		r1.addPassenger(u2);
		r1.setIdRide(1);
		
		ro1 = new RideOld(r1);
		}
		catch(Exception e)
		{
			fail();
		}
	}

	@Test
	public void equalsTest() {
		assertTrue(u1.equals(u4));
		assertFalse(u1.equals(u3));
	}
	
	@Test
	public void addRideTest()
	{
		try {
			u1.addRide(r1);
		} catch (Exception e) {
			fail();
		}
		assertTrue(u1.getRides().size() == 1);
		assertTrue(u1.getRides().contains(r1));
	}
	
	@Test
	public void addRideOld()
	{
		try {
			u1.addRideOld(ro1);
		} catch (Exception e) {
			fail();
		}
		assertTrue(u1.getRidesOld().size() == 1);
		assertTrue(u1.getRidesOld().contains(ro1));
	}
	
	@Test
	public void removeRide()
	{
		try {
			u1.addRide(r1);
			u1.removeRide(r1);
		} catch (Exception e) {
			fail();
		}
		assertTrue(u1.getRides().size() == 0);
	}
	
	@Test
	public void moveToOldRide(){
		try {
			u1.addRide(r1);
			u1.moveToOldRide(r1,ro1);
		} catch (Exception e) {
			fail();
		}
		assertTrue(u1.getRides().size() == 0);
		assertTrue(u1.getRidesOld().size() == 1);
		assertTrue(u1.getRidesOld().contains(ro1));
	}
	
	

}
