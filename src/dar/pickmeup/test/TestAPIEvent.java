package dar.pickmeup.test;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import dar.pickmeup.entities.Event;
import dar.pickmeup.services.EventService;
import dar.pickmeup.util.ExternAPIConnexion;
import dar.pickmeup.util.Tools;

public class TestAPIEvent {

	public static void main(String args[]){
		
		
		try {
			System.out.println("Get All Event Update in API");
			long time = System.currentTimeMillis();
			JSONObject obj = ExternAPIConnexion.EventAPIGet();
			time = System.currentTimeMillis() - time;
			System.out.println("\n\nExec in :"+(time/1000)+"sec");
			
			JSONArray data = obj.getJSONArray("data");
			System.out.println("FIN Size : "+data.length());
			
			ArrayList<Event>events = Tools.getEventList(obj);
			
			for(Event e : events)
				EventService.addEvent(e);
					
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
}
