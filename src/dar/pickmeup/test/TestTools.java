package dar.pickmeup.test;


import org.json.JSONObject;

import dar.pickmeup.services.EventService;
import dar.pickmeup.services.RideOldService;
import dar.pickmeup.services.RideService;
import dar.pickmeup.services.UserService;

public class TestTools {
	
	public static void main (String args []) throws Exception
	{
		
		JSONObject obj;
		
		obj = EventService.getEvent(99);
		System.out.println("===> Event 99");
		System.out.println(obj.toString(3));
		
		obj = RideService.getRide(1005);
		System.out.println("===> Ride 1005");
		System.out.println(obj.toString(3));
		
		obj = RideService.getRide(1007);
		System.out.println("===> Ride 1007");
		System.out.println(obj.toString(3));
		
		obj = UserService.getUserInfo("ar@joanny.com");
		System.out.println("===> ar@joanny.com");
		System.out.println(obj.toString(3));
		System.out.println();
		
		obj = UserService.getUserInfo("wa@cheglal.com");
		System.out.println("===> wa@cheglal.com");
		System.out.println(obj.toString(3));
		System.out.println();
		
		obj = UserService.getUserInfo("ay@sarih.com");
		System.out.println("===> ay@sarih.com");
		System.out.println(obj.toString(3));
		System.out.println();
		
		
	}

}
