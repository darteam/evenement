package dar.pickmeup.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONObject;

import dar.pickmeup.dao.EventDao;
import dar.pickmeup.dao.RideDao;
import dar.pickmeup.dao.RideOldDao;
import dar.pickmeup.dao.UserDao;
import dar.pickmeup.entities.Event;
import dar.pickmeup.entities.Ride;
import dar.pickmeup.entities.RideOld;
import dar.pickmeup.entities.User;
import dar.pickmeup.services.EventService;
/**
 * Class to manage the BDD update
 * @author Arthur
 *
 */
public class BDDUpdate {


	/**
	 * Singleton
	 */
	private  static BDDUpdate singleton;

	private BDDUpdate()
	{

	}
	/**
	 * Init the singleton
	 */
	public static void  init()
	{
		BDDUpdate.singleton = new BDDUpdate();
	}

	/**
	 * Get singleton
	 * @return The Singleton 
	 */
	public static BDDUpdate getSingleton()
	{
		return singleton;
	}
	
	/**
	 * Set A Update which is a timer task for update the BD
	 * @param date Date for the first update
	 * @param time interval between two update
	 */
	public void keepUpdate(Calendar date,long time)
	{
		Timer timer =  new Timer();

		timer.schedule(
				new Update(),
				date.getTime(),
				time
				);
	}

	/**
	 * Update the BDD
	 * @throws Exception If a error occure during update an exception is thrown
	 */
	public static void update() throws Exception
	{
		
		//On recupere tous les evenements qui l'on a pas dans la BD
		JSONObject obj = ExternAPIConnexion.EventAPIGet();
		//On les transforme en object Event
		ArrayList<Event>events = Tools.getEventList(obj);

		System.out.println("UPDATE BDD");
		System.out.println("Elem ++ "+obj.getJSONArray("data").length());

		//Ajout de chaque event dans BD
		for(Event e : events)
			EventService.addEvent(e);

		
		updateRide();
		updateEvent();
		checkEventInfo();
	}
	/**
	 * Mise a jour des Ride dans la BD
	 */
	public static void updateRide()
	{

		List<Ride> list = RideDao.getAll();
		ArrayList<Ride>swap = new ArrayList<Ride>();

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String current = dateFormat.format(new Date(System.currentTimeMillis()));

		//Pour tout les Ride dans la BD
		// Si la date est passe on les met dans la list swap
		if(list == null || list.size() == 0)
			return;
		for(Ride r : list)
		{
			int cpt = r.getDate().compareTo(current);
			if(cpt < 0)
				swap.add(r);
		}
		
		//Pour chaque Ride swap on les transforme en RideOld
		for(Ride r : swap)
		{
			
			System.out.println("Maj Ride "+r.getIdRide()+" r : "+r);
			RideOld ro = new RideOld(r);
			RideOldDao.addRideOld(ro);

			// On met a jour tous les utilisateur qui sont concerne par le trajet
			Set<User> pas = r.getPassengers();
			Set<User> app = r.getApplicants();

			for(User u : pas)
			{
				try{
					u.removeRide(r);
					u.addRideOld(ro);
					UserDao.updateUser(u);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			for(User u : app)
			{
				try{
					u.removeRideWait(r);
					UserDao.updateUser(u);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			//On supprime le Ride de la BD
			RideDao.removeRide(r.getIdRide());
		}
	}

	/**
	 * Update all event in DB
	 */
	public static void updateEvent()
	{
		List<Event> list = EventDao.getAll();
		ArrayList<Event>remove = new ArrayList<Event>();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String current = dateFormat.format(new Date(System.currentTimeMillis()));
		
		//Pour tous les evenements si l'evenement est fini on le supprime
		for(Event e : list)
		{
			int cpt = e.getDateF().compareTo(current);
			if(cpt < 0)
				remove.add(e);
		}

		for(Event e : remove)
			EventDao.removeEvent(e.getIdEvent());	
	}
	
	/**
	 * Update info about event
	 */
	public static void checkEventInfo()
	{
		try {
			//On lance des requetes a l'API pour cerifier les infos des evenements sur la BD
			ExternAPIConnexion.EventAPIcheck();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Class interne pour la misa a a jour de la BD
	 * @author Arthur
	 *
	 */
	public class Update extends TimerTask {

		@Override
		public void run() {
			try {

				update();

			} catch (Exception e) {
				e.printStackTrace();
			}


		}
	}
}
