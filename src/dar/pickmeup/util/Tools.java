package dar.pickmeup.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import dar.pickmeup.dao.EventDao;
import dar.pickmeup.dao.RideDao;
import dar.pickmeup.dao.RideOldDao;
import dar.pickmeup.entities.Event;
import dar.pickmeup.entities.Ride;
import dar.pickmeup.entities.RideOld;
import dar.pickmeup.entities.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class Tools{

	/**
	 * Create a JSONObject from a msg and an error code
	 * @param msg
	 * @param errorCode
	 * @return a json object
	 * Example : 
	 * {
	 * 		error    : msg
	 * 		errorcode: code
	 * }
	 */
	public static JSONObject serviceRefused(Object msg, int errorCode)throws JSONException{
		JSONObject json=new JSONObject();
		json.put("error",msg);
		json.put("errorcode",errorCode);
		return json;
	}
	
	/**
	 * Create JSONObject from msg and boolean 
	 * @param msg
	 * @param state
	 * @return a json object
	 * Example : 
	 * if(state)
	 * {
	 * 	state: SUCCESS
	 *  msg  : msg
	 * }
	 * else
	 * {
	 * 	state: SUCCESS
	 *  msg  : msg
	 * }
	 * 
	 * @throws JSONException
	 */
	public static JSONObject serviceMessage(String msg, boolean state) throws JSONException
	{
		JSONObject obj = new JSONObject();

		if(state)
			obj.put("state","SUCCESS");
		else
			obj.put("state", "FAILURE");
		
		obj.put("msg",msg);
		return obj;
	}

	/**
	 * Create a JSONObject from Event object
	 * @param e
	 * @return
	 * Example : 
	 * {
   	 *		"geo": "[48.856818,2.677004]",
   	 *		"uid": "84694650",
   	 *		"img": "http://cibul.s3.amazonaws.com/event_vannerie-de-jonc-77_152060.jpg",
	 *	    "city": "Saint-Thibault-des-Vignes",
	 *	    "dateF": "2017-12-09",
	 *	    "name": "Vannerie de Jonc (77)",
	 *	    "idEvent": 99,
	 *	    "dateD": "2017-12-09",
	 *	    "dpt": "Seine-et-Marne",
	 *	    "tag": "jonc,chapeau,ruche,mangeoire,vannerie,decoration,noel",
	 *	    "placename": "Etang de la Loy",
	 *	    "adr": "3 Rue Rene Cassin, 77400 Saint-Thibault-des-Vignes"
	 *	}
	 */
	public static JSONObject serviceMessage(Event e)
	{
		JSONObject obj = new JSONObject(e);
		obj.remove("class");
		return obj;
	}
	
	/**
	 * Create a json object from a ride object
	 * @param r
	 * @return
	 *  Example
	 * {
	 * 	idRide    : id
	 *  date      : date
	 *  city      : cite
	 *  time      : time
	 *  owner     : email
	 *  nbPasM	  : nbMax	
	 *  pas       :{[ email1,
	 *  			  email2,
	 *  			  email3
	 *  			]}
	 *  app       :{[ email4,
	 *  			  email5,
	 *  			  email6
	 *  			]}
	 *  eventName : name
	 *  eventCity : cityE
	 *  eventDesc : desc
	 *  eventDateD: dateD
	 *  eventDateF: dateF
	 *  eventAddr : addr
	 *  img 	  : ""
	 * }
	 * @throws JSONException
	 */
	public static JSONObject serviceMessage(Ride r)throws JSONException
	{

		JSONObject obj = new JSONObject();
		JSONArray pas = new JSONArray();
		JSONArray app = new JSONArray();
		
		obj.put("idRide", r.getIdRide());
		obj.put("time", r.getTime());
		obj.put("date", r.getDate());
		obj.put("city", r.getCity());
		obj.put("nbPasM", r.getMaxNbPassengers());
		obj.put("owner", r.getOwner().getEmail());
		
		for(User u : r.getPassengers())
			if(!u.getEmail().equals(r.getOwner().getEmail()))
				pas.put(u.getEmail());
		for(User u : r.getApplicants())
			app.put(u.getEmail());
		
		obj.put("pas", pas);
		obj.put("app",app);
		obj.put("eventName", r.getEvent().getName());
		obj.put("eventDateD", r.getEvent().getDateD());
		obj.put("eventDateF", r.getEvent().getDateD());
		obj.put("eventAddr", r.getEvent().getAdr());
		obj.put("eventDesc", r.getEvent().getdescription());
		obj.put("img", r.getEvent().getImg());
		
		return obj;
	}
	
	/**
	 * Create json object from RideOld Object
	 * @param r
	 * @return
	 *  Example
	 *  {
	 * 	idRide    : id
	 *  date      : date
	 *  city      : cite
	 *  owner     : email
	 *  pas       :{[ email1,
	 *  			  email2,
	 *  			  email3
	 *  			]}
	 *  eventName : name
	 *  eventCity : cityE
	 *  eventDesc : desc
	 *  img 	  : ""
	 * }
	 * @throws JSONException
	 */
	public static JSONObject serviceMessage(RideOld r)throws JSONException
	{

		JSONObject obj = new JSONObject();
		JSONArray pas = new JSONArray();
		
		obj.put("idRide", r.getIdRide());
		obj.put("date", r.getDate());
		obj.put("city", r.getCity());
		obj.put("owner", r.getOwner().getEmail());
		
		for(User u : r.getPassengers())
			if(!u.getEmail().equals(r.getOwner().getEmail()))
				pas.put(u.getEmail());
		
		obj.put("pas", pas);
		obj.put("eventName", r.getNameEvent());
		obj.put("eventCity", r.getCityEvent());
		obj.put("eventDesc", r.getDescEvent());
		obj.put("img","");
	
		
		return obj;
	}

	/**
	 * Create json object from Ride no info about passenger
	 * @param r
	 * @return
	 *  Example
	 *   {
	 *	idRide    : id
	 *  date      : date
	 *  city      : cite
	 *  time      : time
	 *  owner     : email
	 *  nbPasM	  : nbMax	
	 *  eventName : name
	 *  eventCity : cityE
	 *  eventDesc : desc
	 *  eventDateD: dateD
	 *  eventDateF: dateF
	 *  eventAddr : addr
	 *  img 	  : ""
	 * }
	 * @throws JSONException
	 */
	public static JSONObject resumeRide(Ride r)throws JSONException
	{

		JSONObject obj = new JSONObject();
		
		obj.put("idRide", r.getIdRide());
		obj.put("time", r.getTime());
		obj.put("date", r.getDate());
		obj.put("city", r.getCity());
		obj.put("nbPasM", r.getMaxNbPassengers());
		obj.put("owner", r.getOwner().getEmail());
		
		obj.put("eventName", r.getEvent().getName());
		obj.put("eventDateD", r.getEvent().getDateD());
		obj.put("eventDateF", r.getEvent().getDateD());
		obj.put("eventAddr", r.getEvent().getAdr());
		obj.put("eventDesc", r.getEvent().getdescription());
		obj.put("img", r.getEvent().getImg());
		
		return obj;
	}
	
	/**
	 * Create json object from RideOld no info about passenger
	 * @param r
	 * @return
	 *  Example
	 *   {
	 * 	idRide    : id
	 *  date      : date
	 *  city      : cite
	 *  owner     : email
	 *  eventName : name
	 *  eventCity : cityE
	 *  eventDesc : desc
	 *  img 	  : ""
	 * }
	 * @throws JSONException
	 */
	public static JSONObject resumeRideOld(RideOld r)throws JSONException
	{

		JSONObject obj = new JSONObject();
		
		obj.put("idRide", r.getIdRide());
		obj.put("date", r.getDate());
		obj.put("city", r.getCity());
		obj.put("owner", r.getOwner().getEmail());
		
		obj.put("eventName", r.getNameEvent());
		obj.put("eventCity", r.getCityEvent());
		obj.put("eventDesc", r.getDescEvent());
		obj.put("img","");
		
		return obj;
	}

	/**
	 * Create json object from User Object
	 * @param u
	 * @return
	 * Example
	 * {
	 * 	email      : email
	 *  firstName  : fName
	 *  lastName   : lName
	 *  rides      :{[ id1,
	 *  			   id2,
	 *  			   id3
	 *  			]}
	 *  ridesWait  :{[ id4,
	 *  			      id5,
	 *  			      id6
	 *  			]}
	 *  rideOld	  : {[ id7,
	 *  			   id8,
	 *  			   id9
	 *  			]}	
	 *  notifs 	  : {[ msg1,
	 *  			   msg2,
	 *  			   msg3
	 *  			]}
	 * }
	 * @throws JSONException
	 */
	public static JSONObject serviceMessage(User u) throws JSONException
	{

		System.out.println("User "+u);
		JSONObject obj = new JSONObject();

		obj.put("email", u.getEmail());
		obj.put("firstName", u.getFirstName());
		obj.put("lastName", u.getLastName());
		
		JSONArray ridesInfo = new JSONArray();
		JSONArray ridesWaitInfo = new JSONArray();
		JSONArray ridesOldInfo = new JSONArray();
		JSONArray notifs = new JSONArray();
		
		for(Ride r : u.getRides())
			ridesInfo.put(resumeRide(r));

		for(RideOld r : u.getRidesOld())
			ridesOldInfo.put(resumeRideOld(r));

		for(Ride r : u.getRidesWait())
			ridesWaitInfo.put(resumeRide(r));
		 
		obj.put("rides",ridesInfo);
		obj.put("ridesOld", ridesOldInfo);
		obj.put("ridesWait", ridesWaitInfo);
		
		
		for(String notif : u.getNotifs())
		{
			StringTokenizer tk = new StringTokenizer(notif, "-");
			String key = tk.nextToken();
			String value = tk.nextToken();
			
			JSONObject n = new JSONObject();
			n.put("idRide",key);
			n.put("msg", value);
			notifs.put(n);
		}
		
		
		obj.put("notifs", notifs);

		return obj;
	}

	/**
	 * Create list of event from an json object which contains an array on event in json
	 * @param data 
	 * @return The list of event
	 */
	public static ArrayList<Event> getEventList(JSONObject data)
	{
		ArrayList<Event> list = new ArrayList<Event>();
		try {
			JSONArray array = data.getJSONArray("data");
			for(int i = 0;i<array.length();i++)
			{
				JSONObject obj = array.getJSONObject(i);
				String name = obj.getString("title");
				String adr = obj.getString("address");
				String placename = obj.getString("placename");
				String city = obj.getString("city");
				String dpt = obj.getString("department");
				String tag = obj.getString("tags");
				String desc = obj.getString("description");
				String geo = obj.getString("latlon");
				String dateD = obj.getString("date_start");
				String dateF = obj.getString("date_end");
				String img = obj.getString("image");
				String uid = obj.getString("uid");

				Event e = new Event(name, adr, placename, city, dpt, tag, desc, geo, dateD, dateF, img, uid);

				list.add(e);

			}
		} catch (JSONException e) {

			e.printStackTrace();
		}
		return list;
	}
	
	public static boolean checkParams(Map<String,String[]> params)
	{
		for(String key : params.keySet())
		{
			for(String value : params.get(key))
				if(!checkEntry(value))
					return false;
		}
		return true;
		
	}
	
	public static boolean checkEntry(String param)
	{
		if(param.contains("<script>"))
			return false;
		if(param.contains("</script>"))
			return false;
		if(param.contains("INSERT"))
			return false;
		if(param.contains("DELETE"))
			return false;
		if(param.contains(";"))
			return false;
		return true;
	}

}