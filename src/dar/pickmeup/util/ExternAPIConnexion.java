package dar.pickmeup.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import dar.pickmeup.dao.EventDao;
import dar.pickmeup.dao.RideDao;
import dar.pickmeup.dao.UserDao;
import dar.pickmeup.entities.Event;
import dar.pickmeup.entities.Ride;
import dar.pickmeup.entities.User;

/**
 * Class for communication with the API
 * @author Arthur
 *
 */
public class ExternAPIConnexion {


	/**
	 * Get All event in API wich in not in DB
	 * @return A json object wiche contains all event
	 * @throws Exception If there a connexion an exception is thrown
	 */
	public static JSONObject EventAPIGet() throws Exception {

		//On cree la date pour comparer la date des evenements a la date du jour
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String current = dateFormat.format(new Date(System.currentTimeMillis()));

		//On cree l'url pour la requete  a l'API
		String apiUrl = "https://data.iledefrance.fr/api/records/1.0/search?";
		String dataSet = "dataset=evenements-publics-cibul";
		String q="q=date_start>%3D"+current+"+OR+q%3Ddate_end+>%3D"+current;
		String rows="rows=1000";
		String sort="sort=updated_at";
		String pretty="pretty_print=true";


		JSONObject result = new JSONObject();
		JSONArray array = new JSONArray();
		StringBuffer resp = new StringBuffer("");
		boolean stop = true;
		int start_ =0;

		do{
			
			//On fait un requete a l'api pour quelle envoie les evenements range par date d'ajout
			//Pour pas trop recuperer d'evenement on prends 1000 et a chaque tours de boucle on prends les 1000 suivants  
			String start = "start="+start_;
			URL url = new URL(apiUrl+dataSet+"&"+q+"&"+sort+"&"+pretty+"&"+rows+"&"+start);
			System.out.println("Start ==> "+start);

			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

			conn.setRequestMethod("GET");			
			conn.connect();

			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;

			while ((line = rd.readLine()) != null) {
				resp.append(line+"\n");
			}

			conn.disconnect();
			
			//On cree l'objet json obtenue a partir de la requete
			JSONObject doc = new JSONObject(resp.toString());
			JSONArray tab = (JSONArray) doc.get("records");
			
			//On verifie les info obtenue pour chaque evenement
			stop = checkArrayEvent(tab, array);

			start_+=1000;		   
			resp = new StringBuffer("");
				
		}while(!stop);
		
		System.out.println("Stop");
		result.put("data", array);//On renvoie la liste des evenement sous la frome d'un tableau json
		return result;
	}

	/**
	 * Check Info about each event in DB
	 * @throws Exception If an connexion or DB  error occurS an exception is thrown 
	 */
	public static void EventAPIcheck() throws Exception {

		List<Event>events = EventDao.getAll();//On cree la liste de tout les evenements qu'on doit tester
		HashMap<String,Event>map = new HashMap<String,Event>();

		String apiUrl = "https://data.iledefrance.fr/api/records/1.0/search?";
		String dataSet = "dataset=evenements-publics-cibul";
		String rows="rows=100";
		String sort="sort=updated_at";

		StringBuffer resp = new StringBuffer("");
		boolean stop = true;

		do{
			//Du au restrinction sur la taille des url on peut pas faire une seul requete pour tous les evenements
			//On faite donc des requetes pour une centaine d'evenement a la fois
			StringBuffer q = new StringBuffer("q=");
			int i;
			for(i = 0; i<events.size() && i< 100;i++)
			{
				if(q.length() != 2)
					q.append("+OR+");
				q.append("uid:"+events.get(i).getUid());
				//On met dans un map tous les evenements qu'on vas tester
				map.put(events.get(i).getUid(), events.get(i));
			}
			System.out.println(" i : "+i);
			URL  url = new URL(apiUrl+dataSet+"&"+q+"&"+sort+"&"+rows);

			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

			conn.setRequestMethod("GET");			
			conn.connect();

			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;

			while ((line = rd.readLine()) != null) {
				resp.append(line+"\n");
			}

			conn.disconnect();

			JSONObject doc = new JSONObject(resp.toString());
			JSONArray tab = (JSONArray) doc.get("records");
			
			//On check les info tout les evenements recup et tous les evenemetns a tester
			checkInfo(tab, map);

			//On suppr de la liste tout les evenement qu'on a teste
			events.removeAll(map.values());
			map.clear();
			System.out.println("events : "+events.size());
			stop = events.isEmpty();

			resp = new StringBuffer("");

		}while(!stop);
		System.out.println("Stop");
	}

	/**
	 * Check is a date is older than another
	 * @param date 
	 * @param current 
	 * @return true if current is older than date, false otherwise
	 */
	public static boolean isOutOfDate(String date, String current)
	{
		int cmp = date.compareTo(current);
		return cmp < 0;
	}

	/**
	 * Check if the json object got from API is correctly form
	 * @param event the event in json
	 * @param current the current date
	 * @throws Exception If there is a error during the update of the json object an exception is thrown 
	 */
	private static void checkEventMember(JSONObject event,String current) throws Exception
	{

		if(!event.has("title"))
			event.put("title", "");

		if(!event.has("description"))
			event.put("description", "");

		if(!event.has("placename"))
			event.put("placename", "");

		if(!event.has("address"))
			event.put("address", "");

		if(!event.has("city"))
			event.put("city", "");

		if(!event.has("department"))
			event.put("department", "");

		if(!event.has("date_start"))
			event.put("date_start", current);

		if(!event.has("date_end"))
			event.put("date_end", event.getString("date_start"));

		if(!event.has("uid"))
			event.put("uid", "");

		if(!event.has("image"))
			event.put("image", "");

		if(!event.has("tags"))
			event.put("tags", "");

		if(!event.has("latlon"))
			event.put("latlon", "");
		else
			event.put("latlon",event.get("latlon")+"");

	}

	/**
	 * Check if all events in input is well formed and put all event to add in DB in output
	 * @param input 
	 * @param output 
	 * @return true id an event of input is already in DB false otherwise
	 */
	private static boolean checkArrayEvent(JSONArray input,JSONArray output)
	{

		for(int i = 0; i< input.length();i++)
		{
			try{

				JSONObject obj = ((JSONObject)input.get(i)).getJSONObject("fields");
				if(checkEvent(obj))
					output.put(obj);
				else
					return false;
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}

		return true;
	}

	/**
	 * Check if info about event in input are the same that in DB
	 * @param input list of event to check
	 * @param map A map with UID in key set event in value set
	 */
	private static void checkInfo(JSONArray input,HashMap<String,Event>map)
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String current = dateFormat.format(new Date(System.currentTimeMillis()));
		HashMap<String,Event>tmp = new HashMap<String,Event>(map);

		for(int i = 0; i< input.length();i++)
		{
			try{

				JSONObject obj = ((JSONObject)input.get(i)).getJSONObject("fields");
			
				checkEventMember(obj, current);
				
				//Pour chaque event dans input on recup le UID
				String uid = obj.getString("uid");
				//On verifie si les donnees de l'API sont les memes que celles de la BD
				checkInfo(obj,map.get(uid));
				tmp.remove(uid);

			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		if(tmp.size() != 0)
		{
			//Si il reste des evenements dans la map ca veut dire qu'il ne sont plus dans "l'API"
			System.out.println("Des evenement ne sont plus dans l'API......");
			for(Event e : tmp.values())
			{
				System.out.println(e.getName()+" annule");
				//On notifie les utilisateur concerne par "l'annulation de l'evenements"
				eventAbsent(e);
			}

		}
	}

	/**
	 * Check infor about event in json et event object
	 * @param obj
	 * @param e
	 */
	private static void checkInfo(JSONObject obj,Event e){
		try {
			
			//Si il y a un chagement on notifie les utilisateurs concernes
			
			if(!e.getAdr().equals(obj.getString("address")))
			{
				notifyChange(e, "L'adresse a change");
				e.setAdr(obj.getString("address"));
			}
			if(!e.getDateD().equals(obj.getString("date_start")))
			{
				notifyChange(e,"La date de l'evenement a change");
				e.setDateD(obj.getString("date_start"));
			}
			if(!e.getDateF().equals(obj.getString("date_end")))
			{
				notifyChange(e, "La date de l'evenement a change");
				e.setDateF(obj.getString("date_end"));
			}
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
	}
	
	/**
	 * Update DB, notif user that an event is canceled
	 * And remove ride to that event
	 * @param e The event canceled
	 */
	private static void eventAbsent(Event e)
	{
		List<Ride> rides = RideDao.getRideByEvent(e);
		System.out.println("EventAbsent "+ e.getIdEvent()+" : "+rides.size());
		for(Ride r : rides)
		{
			for(User u : r.getPassengers())
			{
				try {
					u.addNotif(r.getIdRide()+"-"+"Evenement et trajet supprimes");
					u.removeRide(r);
					UserDao.updateUser(u);
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
			for(User u : r.getApplicants())
			{
				try {
					u.addNotif(r.getIdRide()+"-"+"L'evenement et trajet supprimes");
					u.removeRideWait(r);
					UserDao.updateUser(u);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
			System.out.println(r.getIdRide() + " trajet supprime");
			RideDao.removeRide(r.getIdRide());
		}
		EventDao.removeEvent(e.getIdEvent());
	}

	/**
	 * Update BDD notify user that a an event has changed
	 * @param e The event which has changed
	 * @param notif The message for users
	 */
	private static void notifyChange(Event e, String notif)
	{
		//On notifi tous les utilisateurs qui participe a un trajet pour l'evenement e
		
		List<Ride> rides = RideDao.getRideByEvent(e);
		if(rides.size() == 0)
			System.out.println(e.getIdEvent() +" - "+ e.getName()+"\nRecherche dans les trajet : "+rides.size());
		for(Ride r : rides)
		{
			System.out.println("Maj ride");
			User owner = r.getOwner();
			owner.addNotif(r.getIdRide()+"-"+notif);
			UserDao.updateUser(owner);
			
			for(User u : r.getPassengers())
			{
				System.out.println(r.getIdRide()+ " - MAJ pass");
				try {
					u.addNotif(r.getIdRide()+"-"+notif);
					u.removeRide(r);
					UserDao.updateUser(u);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
			for(User u : r.getApplicants())
			{
				System.out.println(r.getIdRide()+ " - MAJ pass");
				try {
					u.addNotif(r.getIdRide()+"-"+notif);
					u.removeRideWait(r);
					UserDao.updateUser(u);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	/**
	 * Check if an event should be add to the BDD or not
	 * @param obj The event in json
	 * @return true if the event need to be add to the BD false otherwise
	 * @throws Exception If there is an error with the JSONObject obj an exception is thrown
	 */
	private static boolean checkEvent(JSONObject obj) throws Exception
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String current = dateFormat.format(new Date(System.currentTimeMillis()));

		checkEventMember(obj, current);
		String dStart = obj.getString("date_start");
		String dEnd = obj.getString("date_end");

		boolean outOfDate = isOutOfDate(dStart, current) && isOutOfDate(dEnd,current);
		boolean exist = EventDao.eventExist(obj.getString("uid"));
		if(exist)
			System.out.println("Event exist stop");

		return  !outOfDate && !exist;
	}

}
