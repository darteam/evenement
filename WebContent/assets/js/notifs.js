function showNotif(obj)
{
	var content = document.getElementById("listNotifs");
	var mNotifs = document.getElementById("notifs");
	var uName = document.getElementById("userName");
	uName.innerHTML = obj.email;
	
	while (content.firstChild) {
		content.removeChild(content.firstChild);
	}

	
	var cpt =0;
	for (var id in obj.notifs)
	{
		var notif = obj.notifs[id];
		var item = document.createElement("li");
		var l = document.createElement("a");
		
		l.setAttribute("onClick","removeNotif("+notif.idRide+")");
		l.innerHTML = notif.msg;
		item.appendChild(l);
		content.appendChild(item);
		cpt++;
	}
	
	mNotifs.innerHTML="Mes notifs ("+cpt+")"; 
	if(cpt == 0)
	{
		mNotifs.setAttribute("style","");
	}
	else
	{
		mNotifs.setAttribute("style","color:#880000;");
	}
}

function getNotif()
{

	var option = {
			url: 'user',
			data: {
				mode: "1",
				format: 'json'
			},
			dataType: 'json',
			success: showNotif,
			error: function(data)
			{
				alert("Erreur : Aucune Info Obtenue");
			},
			type: 'GET'
	}; 

	$.ajax(option);
}

function removeNotif(id)
{

	var option = {
			url: 'user',
			data: {
				mode: "4",
				format: 'json',
				idRide : id
			},
			dataType: 'json',
			success: function (data)
			{
				if(data.redirect)
					window.location = "ride.html?idRide="+data.idRide;
				else
					showNotif(data.user);
			},
			error: function(data)
			{
				//alert("Erreur : MAJ Notif\n"+data);
				alert(JSON.stingify(data));
			},
			type: 'POST'
	}; 

	$.ajax(option);
}
function deco()
{
	
	$.get( "test.php", { mode: "5" } );
	var option = {
			url: 'user',
			data: {
				mode: "5",
				format: 'json',
			
			},
			success: function (data)
			{
				window.location = "connexion.html";
			},
			error: function(data)
			{
				window.location = "connexion.html";
			},
			type: 'GET'
	}; 

	$.ajax(option);
}