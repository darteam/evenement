
function $_GET(param) {
	var vars = {};
	window.location.href.replace( location.hash, '' ).replace( 
			/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
			function( m, key, value ) { // callback
				vars[key] = value !== undefined ? value : '';
			}
	);

	if ( param ) {
		return vars[param] ? vars[param] : null;	
	}
	return vars;
}
/**
 * Create the add passenger form
 * */
function createAddForm(obj)
{

	var mainDiv   = document.getElementById("addDiv");


	var div = document.createElement("div");
	div.setAttribute("class","form-group");

	var label = document.createElement("label");
	label.setAttribute("for","dmds");
	label.innerHTML="Demandes"

		var inputSelect = document.createElement("select");
	inputSelect.setAttribute("class","form-control");
	inputSelect.setAttribute("id", "dmds");
	inputSelect.setAttribute("name","idPas");

	for (var id in obj){
		var option = document.createElement("option");
		option.setAttribute("value",obj[id]);
		option.innerHTML=obj[id];
		inputSelect.appendChild(option)
	}



	var inputSubmit = document.createElement("button");
	inputSubmit.setAttribute("class","form-control");
	inputSubmit.setAttribute("type","submit");
	inputSubmit.innerHTML ="Ajouter";
	inputSubmit.setAttribute("style","width:50%");
	inputSubmit.setAttribute("onClick","addPassenger("+$_GET("idRide")+")");

	var inputSubmit2 = document.createElement("button");
	inputSubmit2.setAttribute("class","form-control");
	inputSubmit2.innerHTML="Supprimer";
	inputSubmit2.setAttribute("style","width:50%");
	inputSubmit2.setAttribute("onClick","rmApplicantOwner("+$_GET("idRide")+")");

	div.appendChild(label);
	div.appendChild(inputSelect);
	div.appendChild(document.createElement("br"));
	div.appendChild(inputSubmit);
	div.appendChild(document.createElement("br"));
	div.appendChild(inputSubmit2);

	mainDiv.appendChild(div);
}

/**
 * Create the remove passenger form
 */
function createRMForm(obj)
{
	var mainDiv   = document.getElementById("rmDiv");

	var div = document.createElement("div");
	div.setAttribute("class","form-group");

	var label = document.createElement("label");
	label.setAttribute("for","ajt");
	label.innerHTML="Passagers"

		var inputSelect = document.createElement("select");
	inputSelect.setAttribute("class","form-control");
	inputSelect.setAttribute("id", "ajt");
	inputSelect.setAttribute("name","idPas");

	for (var id in obj){
		var option = document.createElement("option");
		option.setAttribute("value",obj[id]);
		option.innerHTML=obj[id];
		inputSelect.appendChild(option)
	}

	var inputSubmit = document.createElement("button");
	inputSubmit.setAttribute("class","form-control");
	inputSubmit.innerHTML ="Supprimer";
	inputSubmit.setAttribute("style","width:50%");
	inputSubmit.setAttribute("onClick","rmPassenger("+$_GET("idRide")+")");

	div.appendChild(label);
	div.appendChild(inputSelect);
	div.appendChild(document.createElement("br"));
	div.appendChild(inputSubmit);

	mainDiv.appendChild(div);
}

/**
 * Create the delete button
 */
function createDeleteButton()
{
	var aDelete = document.createElement("button");
	aDelete.setAttribute("class","btn btn-primary");
	aDelete.setAttribute("href","#");
	aDelete.innerHTML="Supprimer";	
	document.getElementById("caption").appendChild(aDelete);
}

/**
 * Add form for the ride owner
 */
function addOwnerFrom(obj)
{
	createAddForm(obj.app);
	createRMForm(obj.pas);
	createDeleteButton();

}

/**
 * Create info about the ride for regular user
 */
function addInfo(obj)
{
	var mainDiv   = document.getElementById("rmDiv");

	var form  = document.createElement("form");

	var div = document.createElement("div");
	div.setAttribute("class","form-group");

	var label = document.createElement("label");
	label.setAttribute("for","ajt");
	label.innerHTML="Passagers"

		var inputSelect = document.createElement("select");
	inputSelect.setAttribute("class","form-control");
	inputSelect.setAttribute("id", "ajt");
	inputSelect.setAttribute("name","idPas");


	for (var id in obj.pas){
		var option = document.createElement("option");
		option.setAttribute("value",obj.pas[id]);
		option.innerHTML=obj.pas[id];
		inputSelect.appendChild(option)
	}




	div.appendChild(label);
	div.appendChild(inputSelect);


	form.appendChild(div);
	mainDiv.appendChild(form);

	var aParticiper = document.createElement("button");
	aParticiper.setAttribute("class","btn btn-primary");
	aParticiper.setAttribute("id","button");

	if(obj.isOn)
	{
		aParticiper.innerHTML="Quitter Trajet";	
		aParticiper.setAttribute("onClick","rmPassenger_("+$_GET("idRide")+")");
	}
	else if(obj.isAppli)
	{
		aParticiper.innerHTML="Supprimer Demande";	
		aParticiper.setAttribute("onClick","rmApplicantUser("+$_GET("idRide")+")");
	}
	else	
	{
		aParticiper.innerHTML="Participer";
		aParticiper.setAttribute("onClick","addApplicant("+$_GET("idRide")+")");
	}
	document.getElementById("caption").appendChild(aParticiper);
}

/**
 * Fill the ride page whit the good information
 */
function fillRidePage(obj)
{


	var elem   = document.getElementById("eventName");
	elem.innerHTML = obj.eventName;

	elem   = document.getElementById("userName2");
	elem.innerHTML = obj.owner;

	elem   = document.getElementById("rideVille");
	elem.innerHTML = obj.city;

	elem   = document.getElementById("rideDate");
	elem.setAttribute("value",obj.date);

	elem   = document.getElementById("rideTime");
	elem.setAttribute("value",obj.time);

	elem   = document.getElementById("rideNbPasM");
	elem.setAttribute("value",obj.nbPasM);

	elem   = document.getElementById("description");
	elem.innerHTML = obj.eventName+"<br>"+obj.eventDateF+obj.eventDateF+"<br>"+ obj.eventDesc;

	elem   = document.getElementById("rideImg");
	if(obj.img == "")
		elem.setAttribute("src","assets/img/img2.jpg");
	else
		elem.setAttribute("src",obj.img);

	if(obj.isOwner)
	{
		elem   = document.getElementById("rideDate");
		elem.setAttribute("onkeyup","checkFormDate(this)");


		var button = document.createElement("input")
		button.setAttribute("type","button");
		button.setAttribute("value","Changer Date");
		button.setAttribute("class","form-control")
		button.setAttribute("style","width:10em;")
		button.setAttribute("onclick","updateDate()");

		document.getElementById("spanDate").appendChild(button);

		elem   = document.getElementById("rideTime");
		elem.setAttribute("onkeyup","checkFormDate(this)");


		button = document.createElement("input")
		button.setAttribute("type","button");
		button.setAttribute("value","Changer heure");
		button.setAttribute("class","form-control")
		button.setAttribute("style","width:10em;")
		button.setAttribute("onclick","updateTime()");

		document.getElementById("spanTime").appendChild(button);

		elem   = document.getElementById("rideNbPasM");
		elem.setAttribute("value",obj.time);

		button = document.createElement("input")
		button.setAttribute("type","button");
		button.setAttribute("value","Changer Nb Max");
		button.setAttribute("class","form-control")
		button.setAttribute("style","width:10em;")
		button.setAttribute("onclick","updateNbPasM()");

		document.getElementById("spanNbPasM").appendChild(button);

		addOwnerFrom(obj);
	}
	else
	{
		elem   = document.getElementById("rideDate");
		elem.setAttribute("disabled","disabled");

		elem   = document.getElementById("rideTime");
		elem.setAttribute("disabled","disabled");

		elem   = document.getElementById("rideNbPasM");
		elem.setAttribute("disabled","disabled");

		addInfo(obj);
	}
}

function fillRideOldPage(obj)
{


	var elem   = document.getElementById("eventName");
	elem.innerHTML = obj.eventName;

	elem   = document.getElementById("userName2");
	elem.innerHTML = obj.owner;

	elem   = document.getElementById("rideVille");
	elem.innerHTML = obj.city;

	elem   = document.getElementById("rideDate");
	elem.innerHTML= obj.date;

	elem   = document.getElementById("description");
	elem.innerHTML = obj.eventName+"<br>"+ obj.eventDesc;

	elem   = document.getElementById("rideImg");
	if(obj.img == "")
		elem.setAttribute("src","assets/img/img2.jpg");
	else
		elem.setAttribute("src",obj.img);

	var mainDiv   = document.getElementById("rmDiv");

	var form  = document.createElement("form");

	var div = document.createElement("div");
	div.setAttribute("class","form-group");

	var label = document.createElement("label");
	label.setAttribute("for","ajt");
	label.innerHTML="Passagers"

	var inputSelect = document.createElement("select");
	inputSelect.setAttribute("class","form-control");
	inputSelect.setAttribute("id", "ajt");
	inputSelect.setAttribute("name","idPas");


	for (var id in obj.pas){
		var option = document.createElement("option");
		option.setAttribute("value",obj.pas[id]);
		option.innerHTML=obj.pas[id];
		inputSelect.appendChild(option)
	}

	div.appendChild(label);
	div.appendChild(inputSelect);


	form.appendChild(div);
	mainDiv.appendChild(form);
}

function updatePage()
{
	var id = $_GET("idRide")
	var option = {
		url: 'ride',
		data: {
			mode: "8",
			idRide: id,
			format: 'json'
		},
		dataType: 'json',
		success: fillRidePage,
		error: function(data)
		{
			alert("Erreur : Aucune Info Obtenu");  
		},
		type: 'GET'
	}; 

	$.ajax(option);
}

function updateOldPage()
{
	var id = $_GET("idRide")
	var option = {
		url: 'rideold',
		data: {
			mode: "0",
			idRide: id,
			format: 'json'
		},
		dataType: 'json',
		success: fillRideOldPage,
		error: function(data)
		{
			alert("Erreur : Aucune Info Obtenue");  
		},
		type: 'GET'
	}; 

	$.ajax(option);
}

function addApplicant(id)
{

	var option = {
			url: 'ride',
			data: {
				mode: "3",
				idRide: id,
				format: 'json'
			},
			dataType: 'json',
			success: function(data)
			{
				var button = document.getElementById("button");
				button.innerHTML="Supprimer Demande";
				button.setAttribute("onClick","rmApplicantUser("+id+")");
			},
			error: function(data)
			{
				alert("Erreur : Le passager n'a pas ete ajoute");  
			},
			type: 'POST'
	}; 

	$.ajax(option);

}

function rmApplicantOwner(id)
{
	var e = document.getElementById("dmds");
	var strUser = e.options[e.selectedIndex].value;

	var option = {
			url: 'ride',
			data: {
				mode: "4",
				idRide: id,
				idPas: strUser,
				format: 'json'
			},
			dataType: 'json',
			success: function(data)
			{
				var selectobject=document.getElementById("dmds")
				for (var i=0; i<selectobject.length; i++){
					if (selectobject.options[i].value == strUser )
						selectobject.remove(i);
				}
			},
			error: function(data)
			{
				alert("Erreur : La demande n'a pas ete ajoute");  
			},
			type: 'POST'
	}; 

	$.ajax(option);
	return false;
}


function rmApplicantUser(id)
{

	var option = {
			url: 'ride',
			data: {
				mode: "4",
				idRide: id,
				format: 'json'
			},
			dataType: 'json',
			success: function(data)
			{

				var button = document.getElementById("button");
				button.innerHTML="Participer";
				button.setAttribute("onClick","addApplicant("+id+")");

				location.reload(); 
			},
			error: function(data)
			{
				alert("Erreur : La demande n'a pas ete ajoute");  
			},
			type: 'POST'
	}; 

	$.ajax(option);

	return false;
}

function addPassenger(id)
{
	var e = document.getElementById("dmds");
	var strUser = e.options[e.selectedIndex].value;

	var option = {
			url: 'ride',
			data: {
				mode: "1",
				idRide: id,
				idPas: strUser,
				format: 'json'
			},
			dataType: 'json',
			success: function(data)
			{
				var selectobject=document.getElementById("ajt");
				var opt = document.createElement("option");
				opt.innerHTML=strUser;
				opt.setAttribute("value",strUser);
				selectobject.appendChild(opt);

				var selectobject=document.getElementById("dmds")
				for (var i=0; i<selectobject.length; i++){
					if (selectobject.options[i].value == strUser )
						selectobject.remove(i);
				}
			},
			error: function(data)
			{
				alert("Erreur : Le passager n'a pas ete ajoute");  
			},
			type: 'POST'
	}; 

	$.ajax(option);
	return false;
}

function rmPassenger(id)
{

	var e = document.getElementById("ajt");
	var strUser = e.options[e.selectedIndex].value;

	var option = {
			url: 'ride',
			data: {
				mode: "2",
				idRide: id,
				idPas: strUser,
				format: 'json'
			},
			dataType: 'json',
			success: function(data)
			{
				var selectobject=document.getElementById("ajt");
				for (var i=0; i<selectobject.length; i++){
					if (selectobject.options[i].value == strUser )
						selectobject.remove(i);
				}
			},
			error: function(data)
			{
				alert("Erreur : Le passager n'a pas ete supprimer");  
			},
			type: 'POST'
	}; 

	$.ajax(option);
	return false;
}

function rmPassenger_(id)
{

	var option = {
			url: 'ride',
			data: {
				mode: "2",
				idRide: id,
				format: 'json'
			},
			dataType: 'json',
			success: function(data)
			{

				var button = document.getElementById("button");
				button.innerHTML="Participer";
				button.setAttribute("onClick","addApplicant("+id+")");

				location.reload(); 
			},
			error: function(data)
			{
				alert("Erreur : Le passager n'a pas ete supprime");  
			},
			type: 'POST'
	}; 

	$.ajax(option);
	return false;
}

function showMyRide()
{

	var option = {
			url: 'user',
			data: {
				mode: "1",
				format: 'json'
			},
			dataType: 'json',
			success: fillMyRidesPage,
			error: function(data)
			{
				alert("Erreur : Aucune Info Obtenue");
			},
			type: 'GET'
	}; 

	$.ajax(option);
}

function fillMyRidesPage(obj)
{
	var content = document.getElementById("content");

	var row;
	var cpt =0;
	for (var id in obj.rides){

		var ride = obj.rides[id];

		if((cpt % 3) == 0)
		{
			row = document.createElement("div");
			row.setAttribute("class","row");
			content.appendChild(row);
		}

		var rideDiv = createRideDiv(ride);
		row.appendChild(rideDiv);
		cpt = cpt+1;	
	}
}

function createRideDiv(ride)
{
	var rideDiv = document.createElement("div");
	rideDiv.setAttribute("class","col-md-4");

	var thumb = document.createElement("div");
	thumb.setAttribute("class","thumbnail");

	var img = document.createElement("img");
	if(ride.img == "")
		img.setAttribute("src","assets/img/img2.jpg");
	else
		img.setAttribute("src",ride.img);

	var caption = document.createElement("div")
	caption.setAttribute("class","caption");

	var title = document.createElement("h3");
	title.setAttribute("style","text-align: center;");
	title.innerHTML= ride.eventName;

	var pUser = document.createElement("p");
	pUser.innerHTML= ride.owner;

	var pDate = document.createElement("p");
	pDate.innerHTML = ride.date;

	var desc = document.createElement("p");
	desc.innerHTML = ride.eventDesc;

	var link = document.createElement("a")
	link.setAttribute("class","btn btn-primary");
	link.setAttribute("href","ride.html?idRide="+ride.idRide);
	link.innerHTML="Info";

	caption.appendChild(title);
	caption.appendChild(pUser)
	caption.appendChild(pDate);
	caption.appendChild(desc);
	caption.appendChild(link);

	thumb.appendChild(img);
	thumb.appendChild(caption);

	rideDiv.appendChild(thumb);   

	return rideDiv;
}

function showMyRideOld()
{

	var option = {
			url: 'user',
			data: {
				mode: "1",
				format: 'json'
			},
			dataType: 'json',
			success: fillMyRidesOldPage,
			error: function(data)
			{
				alert("Erreur : Aucune Info Obtenue");  
			},
			type: 'GET'
	}; 

	$.ajax(option);
}

function createRideOldDiv(ride)
{
	var rideDiv = document.createElement("div");
	rideDiv.setAttribute("class","col-md-4");

	var thumb = document.createElement("div");
	thumb.setAttribute("class","thumbnail");

	var img = document.createElement("img");
	if(ride.img == "")
		img.setAttribute("src","assets/img/img2.jpg");
	else
		img.setAttribute("src",ride.img);

	var caption = document.createElement("div")
	caption.setAttribute("class","caption");

	var title = document.createElement("h3");
	title.setAttribute("style","text-align: center;");
	title.innerHTML= ride.eventName;

	var pUser = document.createElement("p");
	pUser.innerHTML= ride.owner;

	var pDate = document.createElement("p");
	pDate.innerHTML= ride.date;

	var desc = document.createElement("p");
	desc.innerHTML = "Terminé";

	var link = document.createElement("a")
	link.setAttribute("class","btn btn-primary");
	link.setAttribute("href","rideOld.html?idRide="+ride.idRide);
	link.innerHTML="Info";

	caption.appendChild(title);
	caption.appendChild(pUser)
	caption.appendChild(pDate);
	caption.appendChild(desc);
	caption.appendChild(link);

	thumb.appendChild(img);
	thumb.appendChild(caption);

	rideDiv.appendChild(thumb);    

	return rideDiv;
}

function fillMyRidesOldPage(obj)
{
	var content = document.getElementById("content");

	var row;
	var cpt =0;
	for (var id in obj.ridesOld){

		var ride = obj.ridesOld[id];

		if((cpt % 3) == 0)
		{
			row = document.createElement("div");
			row.setAttribute("class","row");
			content.appendChild(row);
		}


		var rideDiv = createRideOldDiv(ride);
		row.appendChild(rideDiv);
		cpt = cpt+1;	
	}
}

function fillMyAppliesPage(obj)
{
	var content = document.getElementById("content");

	var row;
	var cpt =0;
	for (var id in obj.ridesWait){

		var ride = obj.ridesWait[id];

		if((cpt % 3) == 0)
		{
			row = document.createElement("div");
			row.setAttribute("class","row");
			content.appendChild(row);
		}

		var rideDiv = createRideDiv(ride);
		row.appendChild(rideDiv);
		cpt = cpt+1;	
	}
}


function showMyApplies()
{

	var option = {
			url: 'user',
			data: {
				mode: "1",
				format: 'json'
			},
			dataType: 'json',
			success: fillMyAppliesPage,
			error: function(data)
			{
				alert("Erreur : Aucune Info Obtenue");  
			},
			type: 'GET'
	}; 

	$.ajax(option);
}


function updateDate()
{
	var dateV = document.getElementById("rideDate").value;
	var id = $_GET("idRide");

	var option = {
			url: 'ride',
			data: {
				mode: "5",
				idRide : id,
				date : dateV,
				format: 'json'
			},
			dataType: 'json',
			success: fillMyAppliesPage,
			error: function(data)
			{
				alert("Erreur : Aucune Info Obtenue");  
			},
			type: 'POST'
	}; 

	$.ajax(option);
}

function updateNbPasM()
{
	var id = $_GET("idRide");
	var nbPas = document.getElementById("rideNbPasM").value;

	var option = {
			url: 'ride',
			data: {
				mode: "6",
				idRide : id,
				nbMax: nbPas,
				format: 'json'
			},
			dataType: 'json',
			success: fillMyAppliesPage,
			error: function(data)
			{
				alert("Erreur : Aucune Info Obtenue");  
			},
			type: 'POST'
	}; 

	$.ajax(option);
}

function updateTime()
{

	var id = $_GET("idRide");
	var timeV = document.getElementById("rideTime").value;


	var option = {
			url: 'ride',
			data: {
				mode: "7",
				idRide : id,
				time: timeV,
				format: 'json'
			},
			dataType: 'json',
			success: fillMyAppliesPage,
			error: function(data)
			{
				alert("Erreur : Aucune Info Obtenue");  
			},
			type: 'POST'
	}; 

	$.ajax(option);
}