$(document).ready(function() {

    //Stops the submit request
    $("#myAjaxRequestForm").submit(function(e){
           e.preventDefault();
    });
   
    //checks for the button click event
    $("#submit_find").click(function(e){
            //get the form data and then serialize that
            dataString = $("#myAjaxRequestForm").serialize();
           
            //get the form data using another method 
//            var countryCode = $("input#countryCode").val(); 
//            dataString = "countryCode=" + countryCode;
           
            //make the AJAX request, dataType is set to json
            //meaning we are expecting JSON data in response from the server
            $.ajax({
                type: "POST",
                url: "SearchRide",
                data: dataString,
                dataType: "json",
               
                //if received a response from the server
                success: function( data, textStatus, jqXHR) {
                    //our country code was correct so we have some information to display
                	
                    	fillSearchResult(data);
                    
                },
               
                //If there was no resonse from the server
                error: function(jqXHR, textStatus, errorThrown){
                     console.log("Something really bad happened " + textStatus);
                      $("#ajaxResponse").html(jqXHR.responseText);
                },
               
                //capture the request before it was sent to server
                beforeSend: function(jqXHR, settings){
                    //adding some Dummy data to the request
                    settings.data += "&dummyData=whatever";
                    //disable the button until we get the response
                    $('#submit_find').attr("disabled", true);
                },
               
                //this is called after the response or error functions are finsihed
                //so that we can take some action
                complete: function(jqXHR, textStatus){
                    //enable the button 
                    $('#submit_find').attr("disabled", false);
                }
     
            });        
    });

});


function fillSearchResult(obj)
{
	var content = document.getElementById("ajaxResponse");
	
	while (content.firstChild) {
	    content.removeChild(content.firstChild);
	}

	var row;
	var cpt =0;

	for (var id in obj.data){
	
		var ride = obj.data[id];
		
		if((cpt % 3) == 0)
		{
			row = document.createElement("div");
			row.setAttribute("class","row");
			content.appendChild(row);
		}

		
		var rideDiv = createResultDiv(ride);
		row.appendChild(rideDiv);
		cpt = cpt+1;	
	}
}


function createResultDiv(ride)
{
	var rideDiv = document.createElement("div");
	rideDiv.setAttribute("class","col-md-4");

	var thumb = document.createElement("div");
	thumb.setAttribute("class","thumbnail");

	var img = document.createElement("img");
	if(ride.img == "")
		img.setAttribute("src","assets/img/img2.jpg");
	else
		img.setAttribute("src",ride.img);

	var caption = document.createElement("div")
	caption.setAttribute("class","caption");

	var title = document.createElement("h3");
	title.setAttribute("style","text-align: center;");
	title.innerHTML= ride.eventName;

	var pUser = document.createElement("p");
	pUser.innerHTML= ride.owner;
	
	var pDate = document.createElement("p");
	pDate.innerHTML = ride.date;
	
	var desc = document.createElement("p");
	desc.innerHTML = ride.eventName+"<br>"+ride.eventDateD+"<br>"+ride.description;

	var link = document.createElement("a")
	link.setAttribute("class","btn btn-primary");
	link.setAttribute("href","ride.html?idRide="+ride.idRide);
	link.innerHTML="Info";

	caption.appendChild(title);
	caption.appendChild(pUser)
	caption.appendChild(pDate);
	caption.appendChild(desc);
	caption.appendChild(link);
	caption.appendChild(createButton(ride));

	thumb.appendChild(img);
	thumb.appendChild(caption);

	rideDiv.appendChild(thumb);   
	
	return rideDiv;
}

function createButton(obj)
{
	var button = document.createElement("button");
	button.setAttribute("id","button_"+obj.idRide);
	button.setAttribute("class","btn btn-primary");
	button.setAttribute("style","margin:1em;");
	
	if(obj.isOn)
	{
		button.innerHTML="Quitter Trajet";	
		button.setAttribute("onClick","rmPassenger_("+obj.idRide+")");
	}
	else if(obj.isAppli)
	{
		button.innerHTML="Supprimer Demande";
		button.setAttribute("onClick","rmApplicantUser("+obj.idRide+")");
	}
	else	
	{
		button.innerHTML="Participer";
		button.setAttribute("onClick","addApplicant("+obj.idRide+")");
	}
	return button;
}


function addApplicant(id)
{

	var option = {
			url: 'ride',
			data: {
				mode: "3",
				idRide: id,
				format: 'json'
			},
			dataType: 'json',
			success: function(data)
			{
				var button = document.getElementById("button_"+id);
				button.innerHTML="Supprimer Demande";
				button.setAttribute("onClick","rmApplicantUser("+id+")");
				//return false;
			},
			error: function(data)
			{
				alert("Erreur : Le passager n'a pas ete ajoute");  
			},
			type: 'POST'
	}; 

	$.ajax(option);
}


function rmApplicantUser(id)
{

	var option = {
			url: 'ride',
			data: {
				mode: "4",
				idRide: id,
				format: 'json'
			},
			dataType: 'json',
			success: function(data)
			{
				var button = document.getElementById("button_"+id);
				button.innerHTML="Participer";
				button.setAttribute("onClick","addApplicant("+id+")");
			},
			error: function(data)
			{
				alert("Erreur : La demande n'a pas ete ajoute");  
			},
			type: 'POST'
	}; 

	$.ajax(option);
	//return false;
}


function rmPassenger_(id)
{

	var option = {
			url: 'ride',
			data: {
				mode: "2",
				idRide: id,
				format: 'json'
			},
			dataType: 'json',
			success: function(data)
			{
				var button = document.getElementById("button_"+id);
				button.innerHTML="Participer";
				button.setAttribute("onClick","addApplicant("+id+")");
		//		return false;
			},
			error: function(data)
			{
				alert("Erreur : Le passager n'a pas ete supprime");
			},
			type: 'POST'
	}; 

	$.ajax(option);
}

function checkFormDate(input)
{
	var value = input.value;
	 if(!input.value.match("^(19|20)[0-9]{2}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$"))
		 input.setAttribute("style","border-color:#FF0000;width:10em;");
	 else
	     input.setAttribute("style","width:10em;border-color:#00FF00;");       
}

function checkFormTime(input)
{
	 if(!input.value.match('(?:[01][0-9]|2[0-3]):(?:[0-5][0-9])'))
		 input.setAttribute("style","border-color:#FF0000;width:10em;");
	 else
		 input.setAttribute("style","width:10em;border-color:#00FF00;"); 
}

