$(document).ready(function() {
	$(function() {
		$("#idNameEvent").autocomplete({
//			 width: 300,
//			 max: 3,
//			 delay: 100,
//			 minLength: 1,
//			 autoFocus: true,
//			 cacheLength: 1,
//			 scroll: true,
//			 highlight: false,
			source : function(request, response) {
				$.ajax({
					url : "AutocompletionEventName",
					type : "GET",
					dataType : "json",
					data : {
						term : request.term
					},
					success : function(data) {
						response(data);
					}
				});
			},
//			select: function( event, ui ) {
//		         alert( ui.item.value );
//		         // Your code
//		         return false;
//		      }
			change: function (event, ui) {
		        if (ui.item == null || ui.item == undefined) {
		            $("#idNameEvent").val("");
//		            $("#idNameEvent").attr("disabled", false);
		        } else {
//		            $("#idNameEvent").attr("disabled", true);
		        }
		    }

		});
	});
});